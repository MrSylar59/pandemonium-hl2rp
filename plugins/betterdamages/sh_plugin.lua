PLUGIN.name = "Dégâts Réalistes"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Modifie quelques dégâts pour les rendres réalistes."

nut.config.add("realisticExplosions", true, "Active ou désactive le fait de tomber après une explosion", nil, {
	category = "Better Damages"
})
nut.config.add("hemorragies", true, "Active ou désactive la possibilité d'avoir une hémorragie", nil, {
	category = "Better Damages"
})
nut.config.add("fallOnShot", true, "Active ou désactive le fait de tomber quand on se fait tirer dessus", nil, {
	category = "Better Damages"
})
nut.config.add("dropOnShot", true, "Active ou désactive le fait de lâcher son arme quand on se fait tirer sur les bras", nil, {
	category = "Better Damages"
})

if (SERVER) then

	local playerMeta = FindMetaTable("Player")

	local function DoBleed(ent)
		if (!IsValid(ent) or (ent:IsPlayer() and !ent:Alive())) then
			return
		end

		local jitter = VectorRand() * 30
		jitter.z = 20

		nut.util.drawDown(ent:GetPos() + jitter, "Blood", ent)
	end

	local function IsConfigActive(conf) 
		return nut.config.get(conf)
	end

	local function GetRandomChance(client)
		local stm = client:getLocalVar("stm", 0)
		local health = client:Health()
		local armor = client:Armor()

		return (math.random(200, 400) - (stm + health + 2*armor))
	end

	function playerMeta:MakeBleed() 
		timer.Create("nut_bleeding_"..self:SteamID64(), math.random(60, 120), 0, function() 
			if (self.bleeding == true and self:Health() > 0) then
				local pos = self:GetPos() + Vector(math.random(0, 30), math.random(0, 30), math.random(0, 30))
				local effectData = EffectData()

				effectData:SetOrigin(pos)
				util.Effect("BloodImpact", effectData)
				DoBleed(self)
				self:notifyLocalized("Vous vous videz de votre sang.")
				self:TakeDamage(10, nil, nil)
			else
				timer.Destroy("nut_bleeding_"..self:SteamID64())
				self.bleeding = nil
			end
		end)
	end

	function PLUGIN:EntityTakeDamage(ent, dmg) 
		if (IsValid(ent) and ent:IsPlayer() and ent:Health() > 5 and ent:GetMoveType() != MOVETYPE_NOCLIP and !IsValid(ent.nutRagdoll)) then
			if (!ent:isZombie()) then
				if (dmg:IsExplosionDamage() and IsConfigActive("realisticExplosions") and ent:Armor() <= 50) then
					ent:setRagdolled(true, math.random(15, 30))
				end

				if (!dmg:IsExplosionDamage() and dmg:IsFallDamage()) then
					netstream.Start(ent, "nutShallFall", dmg:GetDamage())
				end

				if (!dmg:IsExplosionDamage() and !dmg:IsFallDamage()) then
					if (IsConfigActive("hemorragies") and ent:Armor() <= 0 and GetRandomChance(ent) >= 180 and !ent.bleeding and ent:Armor() <= 0) then
						ent.bleeding = true
						ent:notifyLocalized("Vous sentez que vous vous videz de votre sang.")
						ent:MakeBleed()
					end

					if (IsConfigActive("fallOnShot") and GetRandomChance(ent) >= 150 and ent:Armor() <= 20) then
						ent:setRagdolled(true, math.random(20, 50))
					end

					if (IsConfigActive("dropOnShot" and (GetRandomChance(ent) - (ent:getLocalVar("str", 0) + ent:getLocalVar("end", 0))/2) >= 150 and ent:Armor() <= 20)) then
						if (ent:LastHitGroup() == (HITGROUP_LEFTARM or HITGROUP_RIGHTARM)) then
							for k, v in pairs(ent:getChar():getInv():getItems()) do
								if (v.isWeapon and v.class == ent:GetActiveWeapon():GetClass()) then
									nut.item.spawn(v.uniqueID, ent:GetShootPos(), function() 
										v:remove()
									end, Angle(), v.data)
								end
							end
						end
					end
				end
			end
		end
	end

	function PLUGIN:PlayerSpawn(client) 
		if (timer.Exists("nut_bleeding_"..client:SteamID64())) then
			timer.Destroy("nut_bleeding_"..client:SteamID64())
			client.bleeding = nil
		end
	end

else

	netstream.Hook("nutShallFall", function(time) 
		timer.Simple(0.2, function() 
			if (LocalPlayer():Alive()) then
				nut.command.send("fallover", time)
			end
		end)
	end)

end