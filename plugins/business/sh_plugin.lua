PLUGIN.name = "Permis pour le Business"
PLUGIN.desc = "Ajoute des permis qui sont achetables pour vendre certains objets."
PLUGIN.author = "Chessnut"

function PLUGIN:CanPlayerUseBusiness(client, uniqueID)
	local itemTable = nut.item.list[uniqueID]

	if (itemTable and itemTable.permit) then
		if (!client:getChar():getInv():hasItem("permit_"..itemTable.permit)) then
			return false
		end
	end
end