ITEM.name = "Tenue de Loyaliste"
ITEM.desc = "Une tenue sobre vous distinguant légèrement de la foule.\nElle est également doublée pour porter chaud."
ITEM.category = "Vêtement"
ITEM.model = "models/tnb/items/aphelion/wintercoat.mdl"
ITEM.width = 2
ITEM.height = 1
ITEM.armor = 0
ITEM.outfitCategory = "body"
-- ITEM.newModel (string) : remplace le modèle du joueur
-- ITEM.bodyGroups (table) : remplace un ou plusieurs bodygroups du joueur
-- ITEM.attribBoosts (table) : permet de booster un attribut

if (CLIENT) then

	function ITEM:paintOver(item, w, h) 
		if (item:getData("equip")) then
			surface.SetDrawColor(110, 255, 110, 100)
			surface.DrawRect(w - 14, h - 14, 8, 8)
		end
	end

end

function ITEM:GetArmorStatus() 
	return self:getData("armorStatus", self.armor)
end

function ITEM:removeParts(client) 
	local char = client:getChar()

	self:setData("equip", false)

	if (char:getData("oldModel")) then
		char:setModel(char:getData("oldModel"))
		char:setData("oldModel", nil)
	end

	for k, v in pairs(self.bodyGroups or {}) do
		local i = client:FindBodygroupByName(k)

		if (i > -1) then
			client:SetBodygroup(i, 0)

			local groups = char:getData("groups", {})

			if (groups[i]) then
				groups[i] = nil
				char:setData("groups", groups)
			end
		end
	end

	if (self.attribBoosts) then
		for k, _ in pairs(self.attribBoosts) do
			char:removeBoost(self.uniqueID, k)
		end
	end

	self:setData("armorStatus", client:Armor())
	client:SetArmor(0)
end

ITEM:hook("drop", function(item) 
	if (item:getData("equip")) then
		item:removeParts(item.player)
	end
end)

ITEM.functions.EquipUn = {
	name = "Retirer",
	tip = "equipTip",
	icon = "icon16/cross.png",
	onRun = function(item) 
		item:removeParts(item.player)
		item.player:EmitSound("npc/combine_soldier/gear"..math.random(3, 6)..".wav")

		return false
	end,
	onCanRun = function(item) 
		return (!IsValid(item.entity) and item:getData("equip") == true)
	end
}

ITEM.functions.Equip = {
	name = "Enfiler",
	tip = "equipTip",
	icon = "icon16/tick.png",
	onRun = function(item) 
		local client = item.player
		local char = client:getChar()
		local items = char:getInv():getItems()

		for k, v in pairs(items) do
			if (v.id != item.id) then
				local itemTable = nut.item.instances[v.id]

				if (v.outfitCategory == item.outfitCategory and itemTable:getData("equip")) then
					client:notifyLocalized("Vous portez déjà ce type de vêtement.")

					return false
				end
			end
		end

		item:setData("equip", true)
		client:EmitSound("npc/combine_soldier/gear"..math.random(3, 6)..".wav")

		if (item.newModel) then
			char:setData("oldModel", char:getData("oldModel", client:GetModel()))
			char:setModel(item.newModel)
		end

		if (item.bodyGroups) then
			local groups = {}

			for k, v in pairs(item.bodyGroups) do
				local i = client:FindBodygroupByName(k)

				if (i > -1) then
					groups[i] = v
				end
			end

			local newGroups = char:getData("groups", {})

			for k, v in pairs(groups) do
				newGroups[k] = v
				client:SetBodygroup(k, v)
			end

			if (table.Count(newGroups) > 0) then
				char:setData("groups", newGroups)
			end
		end

		if (item.attribBoosts) then
			for k, v in pairs(item.attribBoosts) do
				char:addBoost(item.uniqueID, k, v)
			end
		end

		if (item.armor and item.armor > 0) then
			client:SetArmor(item:GetArmorStatus())
		end

		return false
	end,
	onCanRun = function(item) 
		return (!IsValid(item.entity) and item:getData("equip") != true)
	end
}

function ITEM:onCanBeTransfered(oldInv, newInv)
	if (newInv and self:getData("equip")) then
		return newInv:getId() == 0
	end

	return true
end