ITEM.name = "Tenue de Loyaliste"
ITEM.desc = "Une tenue sobre vous distinguant légèrement de la foule.\nElle est également doublée pour porter chaud."
ITEM.model = "models/tnb/items/aphelion/wintercoat.mdl"
ITEM.width = 2
ITEM.height = 1
ITEM.bodyGroups = {
	["torso"] = 2,
	["legs"] = 1
}