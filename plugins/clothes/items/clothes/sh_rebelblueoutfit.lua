ITEM.name = "Tenue de Rebelle Bleue"
ITEM.desc = "Une tenue de rebelle bleue. Elle restreint légèrement vos mouvement,\nmais vous vous sentez plus en sécurité avec."
ITEM.model = "models/tnb/items/aphelion/shirt_rebel1.mdl"
ITEM.skin = 1
ITEM.width = 2
ITEM.height = 1
ITEM.armor = 35
ITEM.bodyGroups = {
	["torso"] = 5,
	["legs"] = 3
}