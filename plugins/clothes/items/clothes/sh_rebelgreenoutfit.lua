ITEM.name = "Tenue de Rebelle Verte"
ITEM.desc = "Une tenue de rebelle verte. Elle restreint légèrement vos mouvement,\nmais vous vous sentez plus en sécurité avec."
ITEM.model = "models/tnb/items/aphelion/shirt_rebel1.mdl"
ITEM.skin = 0
ITEM.width = 2
ITEM.height = 1
ITEM.armor = 35
ITEM.bodyGroups = {
	["torso"] = 6,
	["legs"] = 4
}