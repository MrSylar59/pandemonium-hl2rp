ITEM.name = "Tenue de Médecin Rebelle"
ITEM.desc = "Une tenue de médecin. Elle restreint très peu vos mouvement,\nmais elle semble moins épaisse qu'une tenue classique."
ITEM.model = "models/tnb/items/aphelion/shirt_rebel1.mdl"
ITEM.skin = 0
ITEM.width = 2
ITEM.height = 1
ITEM.armor = 20
ITEM.bodyGroups = {
	["torso"] = 8,
	["legs"] = 3
}