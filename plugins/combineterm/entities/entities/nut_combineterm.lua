AddCSLuaFile()

local PLUGIN = PLUGIN

ENT.Type = "anim"
ENT.PrintName = "Terminal Combine"
ENT.Category = "HL2 RP"
ENT.Author = "MrSylar59"
ENT.Spawnable = true
ENT.AdminOnly = true

if (SERVER) then

	function ENT:Initialize() 
		self:SetModel("models/props_combine/combine_interface002.mdl")
		self:SetSolid(SOLID_VPHYSICS)
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		self:SetUseType(SIMPLE_USE)

		self.isActived = false

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:Wake()
		end

		PLUGIN:saveCombineTerms()
	end

	function ENT:Use(activator) 
		local data = (activator:getChar():getData("txt") or SCHEMA.defaultData)

		if (!activator:isCombine() and !activator:isCwu() and !self.isActived) then
			self.isActived = true
			self:EmitSound("ambient/machines/combine_terminal_idle3.wav")
			nut.chat.send(activator, "it", "Une barre de chargement apparaît sur la machine : <:: Chargement... ::>")
			timer.Simple(10, function() 
				nut.chat.send(activator, "it", "L'écran de la machine se met à jour : <:: Bonjour ".. activator:Name() ..". ::>")
				nut.chat.send(activator, "it", "<:: Voici votre statut en ville :\n".. data:sub(0, data:find("I")-3) ..". ::>")
				self.isActived = false
			end)
		elseif (!activator:isCombine() and !activator:isCwu() and self.isActived) then
			activator:notifyLocalized("Ce terminal est en cours d'utilisation.")
		else
			activator:notifyLocalized("Ce terminal est réservé à l'usage des Citoyens.")
		end
	end

	function ENT:OnRemove() 
		if (!nut.shuttingDown and !nut.cleaningUp) then
			PLUGIN:saveCombineTerms()
		end
	end

end