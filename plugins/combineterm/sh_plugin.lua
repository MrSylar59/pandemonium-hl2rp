PLUGIN.name = "Terminal Combine"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute un terminal combine pour que les citoyens puissent vérifier leur status."

if (SERVER) then

	function PLUGIN:saveCombineTerms() 
		local data = {}

		for k, v in pairs(ents.FindByClass("nut_combineterm")) do
			data[#data + 1] = {v:GetPos(), v:GetAngles()}
		end

		self:setData(data)
	end

	function PLUGIN:LoadData() 
		local data = self:getData() or {}

		for k, v in pairs(data) do
			local entity = ents.Create("nut_combineterm")
			entity:SetPos(v[1])
			entity:SetAngles(v[2])
			entity:Spawn()

			local physObj = entity:GetPhysicsObject()
			if (IsValid(physObj)) then
				physObj:EnableMotion(false)
			end
		end
	end

end