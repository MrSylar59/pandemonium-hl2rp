local PANEL = {} -- La fenêtre de configuration

	function PANEL:Init() 
		self:SetTitle("Config Ordinateur")
		self:SetPos(ScrW()/2, ScrH()/2)
		self:SetSize(ScrW() * 400/1920, ScrH() * 300/1080)
		self:MakePopup()

		local noticePanel = self:Add("nutNoticeBar")
		noticePanel:Dock(TOP)
		noticePanel:DockMargin(0, 0, 0, 5)
		noticePanel:setType(7)
		noticePanel:setText("Entrez dans le champ le lien vers le site web. (Faire Entrée)")
		self.lastY = noticePanel:GetTall() + 100

		local label = self:Add("DLabel")
		label:SetPos(25, self.lastY)
		label:SetText("URL :")
		label:SizeToContents()
		label:SetTextColor(color_white)
		label:SetExpensiveShadow(2, Color(0, 0, 0, 200))
		self.lastY = self.lastY + label:GetTall() + 5

		local entry = self:Add("DTextEntry")
		entry:SetWide(self:GetWide() - 50)
		entry:SetPos(self:GetWide()/2 - entry:GetWide()/2, self.lastY)
		entry:SetTall(20)

		local button = self:Add("DButton")
		button:Dock(BOTTOM)
		button:DockMargin(50, 0, 50, 10)
		button:SetText("Confirmer")
		button:SetTall(25)
		button:SetTextColor(color_white)
		button.DoClick = function()
			self:updateURL(entry:GetValue())
			self:Close()
		end

	end

	function PANEL:Think()
		if (!self:IsActive()) then
			self:MakePopup()
		end
	end

	function PANEL:updateURL(url) 
		netstream.Start("editURL", url)
	end
vgui.Register("nut_ComputerConf", PANEL, "DFrame")


local PANEL_C = {} -- La fenêtre classique pour le joueur

	function PANEL_C:Init() 
		self:SetTitle("Ordinateur")
		self:SetPos(ScrW()/2, ScrH()/2)
		self:SetSize(ScrW() * 1280/1920, ScrH() * 720/1080)
		self:MakePopup()

		self.html = self:Add("DHTML")
		self.html:Dock(FILL)
		self.html:DockMargin(0, 0, 0, 0)

		self.ctrl = self:Add("DHTMLControls")
		self.ctrl:Dock(TOP)
		self.ctrl:SetHTML(self.html)
		self.ctrl.AddressBar.OnEnter = function() 
			if (true) then -- TODO: Bloquer les sites autres que Pandémonium
				self.html:StopLoading()
				self.html:OpenURL(ctrl.AddressBar:GetValue())
			else 
				-- TODO : Rediriger vers une fausse page 404
			end
		end

		self.html:MoveBelow(self.ctrl)

	end

	function PANEL_C:SetURL(entity) 
		self.ctrl.AddressBar:SetText(entity.url)
		self.html:OpenURL(entity.url)
	end

	function PANEL_C:Think()
		if (!self:IsActive()) then
			self:MakePopup()
		end
	end
vgui.Register("nut_Computer", PANEL_C, "DFrame")