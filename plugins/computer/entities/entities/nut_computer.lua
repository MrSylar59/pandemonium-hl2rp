AddCSLuaFile()

local PLUGIN = PLUGIN

ENT.Type = "anim"
ENT.PrintName  = "Ordinateur"
ENT.Category = "HL2 RP"
ENT.Author = "MrSylar59"
ENT.Spawnable = true
ENT.AdminOnly = true

if (SERVER) then

	function ENT:AddChild(model, pos) 
		local child = ents.Create("prop_dynamic")
		local ang = self:GetAngles()

		self:SetAngles(Angle(0, 0, 0))
		child:SetModel(model)
		child:SetPos(self:GetPos() + pos)
		child:SetAngles(self:GetAngles())
		child:SetParent(self)
		self:DeleteOnRemove(child)
		child:Spawn()
		self:SetAngles(ang)
	end

	function ENT:Initialize()
		self:SetModel("models/props_lab/monitor01a.mdl")
		self:SetSolid(SOLID_VPHYSICS)
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		self:SetUseType(SIMPLE_USE)

		self.url = "https://www.google.fr/"

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:Wake()
		end

		self:AddChild("models/props_c17/computer01_keyboard.mdl", Vector(25, 0, -13))
		self:AddChild("models/props_lab/harddrive02.mdl", Vector(0, -20, -3))
	end

	function ENT:Use(activator) 
		activator.computer = self
		netstream.Start(activator, "nut_ComputerWindow", self:EntIndex(), self.url)
	end

else
	netstream.Hook("nut_ComputerWindow", function(index, url)
		local entity = Entity(index)

		entity.url = url

		if (!IsValid(entity)) then
			return
		end

		if (IsValid(nut.gui.computer)) then
			nut.gui.computer:Remove()
			return
		end
		nut.gui.computer = vgui.Create("nut_Computer")
		nut.gui.computer:Center()
		nut.gui.computer:SetURL(entity)
	end)
end