PLUGIN.name = "Ordinateurs"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute des ordinateurs consultables en jeu."

nut.util.include("cl_menu.lua")

nut.command.add("configcomputer", {
	adminOnly = true,
	onRun = function(client, arguments) 
		local data = {}
			data.start = client:GetShootPos()
			data.endpos = data.start + client:GetAimVector()*96
			data.filter = client
		local trace = util.TraceLine(data)
		local ent = trace.Entity

		if (IsValid(ent) and ent:GetClass() == "nut_computer") then
			if (SERVER) then

				netstream.Start(client, "nut_ComputerConfigWindow", client, ent)

			end
		else
			client:notifyLocalized("Vous ne regardez pas un ordinateur valide.")
		end
	end
})

if (SERVER) then

	netstream.Hook("editURL", function(client, url) 
		local entity = client.computer

		if (!IsValid(entity)) then
			return
		end

		entity.url = url
	end)

else

	netstream.Hook("nut_ComputerConfigWindow", function() 
		if (IsValid(nut.gui.computerConf)) then
			nut.gui.computerConf:Remove()
			return
		end
		nut.gui.computerConf = vgui.Create("nut_ComputerConf")
		nut.gui.computerConf:Center()
	end)

end