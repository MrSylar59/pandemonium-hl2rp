local PANEL = {}

	function PANEL:Init() 
		self:SetPos(ScrW() * 0.375, ScrH() * 0.125)
		self:SetSize(ScrW() * nut.config.get("sbHeight")/2, ScrH() * nut.config.get("sbWidth")*1.5)
		self:MakePopup()
		self:SetTitle("Table de Craft")

		local noticePanel = self:Add("nutNoticeBar")
		noticePanel:Dock(TOP)
		noticePanel:DockMargin(0, 0, 0, 5)
		noticePanel:setType(7)
		noticePanel:setText("Vous pouvez fabriquer des objets en cliquant sur leurs icones.")
		

		self.list = self:Add("DScrollPanel")
		self.list:Dock(FILL)
		self.list:SetDrawBackground(true)

		self.categories = {}

		for class, itemTable in SortedPairs(RECIPES:GetAll()) do -- On remplie notre liste avec les catégories et les icones des crafts
			
			if (RECIPES:CanCraft(LocalPlayer(), itemTable.uid) == 0) then
				local category = itemTable.category
				local category2 = string.lower(category)

				if(!self.categories[category2]) then
					local category3 = self.list:Add("DCollapsibleCategory")
					category3:Dock(TOP)
					category3:SetLabel(category)
					category3:DockMargin(5, 5, 5, 5)
					category3:SetPadding(5)

					local list = vgui.Create("DIconLayout")
					list.Paint = function(list, w, h)
						surface.SetDrawColor(0, 0, 0, 25)
						surface.DrawRect(0, 0, w, h)
					end
					category3:SetContents(list)

					local icon = list:Add("SpawnIcon")
					icon:SetModel(itemTable.model or "models/props_lab/box01a.mdl")
					icon.PaintOver = function(icon, w, h)
						surface.SetDrawColor(0, 0, 0, 45)
						surface.DrawOutlinedRect(1, 1, w - 2, h - 2)
					end

					local text = "Fabriquer ".. itemTable.name .."\n".. itemTable.desc .."\n\nExigé :\n".. tostring(itemTable.items[0]):gsub("nil", "")
					local cnt = 0
					local brk = "\n"

					for itc, qua in pairs(itemTable.items) do
						cnt = cnt + 1
						if (cnt == table.Count(itemTable.items)) then brk = "" end
							local tblItem = nut.item.list[itc]
							if tblItem then
								text = text .. tblItem.name .. " x ".. qua .. brk
							end
						end
					icon:SetToolTip(text)

					icon.DoClick = function(panel)
						if (icon.disabled) then
							return
						end
						net.Start("nut_CraftItem")
							net.WriteString(class)
						net.SendToServer()
						icon.disabled = true
						icon:SetAlpha(70)
						timer.Simple(1, function()
							if (IsValid(icon)) then
								icon.disabled = false
								icon:SetAlpha(255)
							end
						end)	
					end

					category3:InvalidateLayout(true)
					self.categories[category2] = {list = list, category = category3, panel = panel}

				else
					local list = self.categories[category2].list
					local icon = list:Add("SpawnIcon")
					icon:SetModel(itemTable.model or "models/props_lab/box01a.mdl")

					local text = "Fabriquer ".. itemTable.name .."\n".. itemTable.desc .."\n\nExigé :\n".. itemTable.items
					local cnt = 0
					local brk = "\n"

					for itc, qua in pairs(itemTable.items) do
						cnt = cnt + 1
						if (cnt == table.Count(itemTable.items)) then brk = "" end
						local tblItem = nut.item.list[itc]
						if tblItem then
							text = text .. tblItem.name .. " x ".. qua .. brk
						end
					end
					icon:SetToolTip(text)

					icon.PaintOver = function(icon, w, h)
						surface.SetDrawColor(0, 0, 0, 45)
						surface.DrawOutlinedRect(1, 1, w - 2, h - 2)
					end

					icon.DoClick = function(panel)
						if (icon.disabled) then
							return
						end
						net.Start("nut_CraftItem")
							net.WriteString(class)
						net.SendToServer()
						icon.disabled = true
						icon:SetAlpha(70)
						timer.Simple(1, function()
							if (IsValid(icon)) then
								icon.disabled = false
								icon:SetAlpha(255)
							end
						end)	
					end
				end
			end
		end
	end

	function PANEL:Think() 
		if (!self:IsActive()) then
			self:MakePopup()
		end
	end
vgui.Register("nut_Crafting", PANEL, "DFrame")


function PLUGIN:CreateMenuButtons(menu, addButton) 
	if (self.IsEnabled) then
		addButton("crafting", "Craft", function()
			nut.gui.crafting = vgui.Create("nut_Crafting", menu)
			menu:SetCurrentMenu(nut.gui.crafting)
		end)
	end
end