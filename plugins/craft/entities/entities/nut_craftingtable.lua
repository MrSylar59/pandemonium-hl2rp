ENT.Type = "anim"
ENT.PrintName = "Table de Craft"
ENT.Author = "MrSylar59"
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.Category = "HL2 RP"
ENT.RenderGroup 		= RENDERGROUP_BOTH

if (SERVER) then -- On est côté serveur

	function ENT:Initialize()
		self:SetModel("models/props_c17/FurnitureTable002a.mdl")
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetUseType(SIMPLE_USE)
		local physicsObject = self:GetPhysicsObject()
		if ( IsValid(physicsObject) ) then
			physicsObject:Wake()
		end
		hook.Run("shallSaveCraftTables")
	end

	function ENT:Use(activator) 
		-- On envoie un message au côté client
		netstream.Start(activator, "nut_CraftWindows", activator)
	end

	function ENT:OnRemove()
		if (!nut.shuttingDown and !nut.cleaningUp) then
			hook.Run("shallSaveCraftTables")
		end
	end

else -- On est côté client

	-- On réceptionne le message du serveur
	netstream.Hook("nut_CraftWindows", function(client, data)
		if (IsValid(nut.gui.crafting)) then
			nut.gui.crafting:Remove()
			return
		end
		surface.PlaySound("items/ammocrate_close.wav")
		nut.gui.crafting = vgui.Create("nut_Crafting")
		nut.gui.crafting:Center()
	end)

	function ENT:Draw()
		self:DrawModel()
	end

	ENT.DisplayVector = Vector( 0, 0, 18.5 )
	ENT.DisplayAngle = Angle( 0, 90, 0 )
	ENT.DisplayScale = .5

	function ENT:DrawTranslucent() 
		local pos = self:GetPos() + self:GetUp() * self.DisplayVector.z + self:GetRight() * self.DisplayVector.x + self:GetForward() * self.DisplayVector.y
		local ang = self:GetAngles()

		ang:RotateAroundAxis( self:GetRight(), self.DisplayAngle.pitch )
		ang:RotateAroundAxis( self:GetUp(),  self.DisplayAngle.yaw )
		ang:RotateAroundAxis( self:GetForward(), self.DisplayAngle.roll )

		-- On commence à dessiner sur la table
		cam.Start3D2D(pos, ang, self.DisplayScale)
			surface.SetDrawColor(0, 0, 0)
			surface.SetTextColor(255, 255, 255)
			surface.SetFont("ChatFont")

			local textSize = {x = 10, y = 10}
			textSize.x, textSize.y = surface.GetTextSize("Table de Craft")

			surface.SetTextPos(-textSize.x/2, -textSize.y/2)
			textSize.x = textSize.x + 20
			textSize.y = textSize.y + 15

			surface.DrawText("Table de Craft")
		cam.End3D2D()
		-- On arrête de dessiner sur la table
	end

end