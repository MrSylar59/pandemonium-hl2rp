PLUGIN.name = "Système de Craft"
PLUGIN.author = "Black Tea & MrSylar59"
PLUGIN.desc = "Ajoute un système de craft complet"

RECIPES = {}
RECIPES.recipes = {}

function RECIPES:Register(tbl) 
	if (!tbl.CanCraft) then
		function tbl:CanCraft(client) -- On vérifie si le joueur peut craft un objet
			local foundItems = 0

			for k, v in pairs(self.items) do -- On lit la liste des objets nécessaire à la création d'un nouvel objet

				for i, j in pairs(client:getChar():getInv():getItems()) do -- On lit l'inventaire du joueur
					if (j.name == nut.item.list[k].name) then -- Si on trouve deux objets du même nom, c'est un ingrédient
						foundItems = foundItems + 1
					end
				end

				if (!client:getChar():getInv():hasItem(k) or foundItems < v) then
					return false -- Le joueur ne peut pas craft s'il n'a pas les objets nécessaire à la fabrication
				end
			end

			return true
		end
	end

	if (!tbl.ProcessCraftItems) then
		function tbl:ProcessCraftItems(client) -- On gère la création des objets

			client:EmitSound("items/ammo_pickup.wav")

			for k, v in pairs(self.items) do
				for i = 1, v do -- v représente le nombre d'objets qui sera consommé lors du craft
					if client:getChar():getInv():hasItem(k) then
						client:getChar():getInv():getItemByID(client:getChar():getInv():hasItem(k):getID()):remove() -- On supprime l'objet utilisé
					end
				end
			end

			for k, v in pairs(self.result) do
				for i = 1, v do -- v représente le nombre d'objets créés lors du craft
					nut.item.spawn(k, client:getItemDropPos()) -- On fait apparaître un objet au dessus de la table de craft
				end
			end
			client:notifyLocalized("Vous avez fabriqué %s.", self.name)
		end
	end

	self.recipes[tbl.uid] = tbl
end

nut.util.include("sh_recipies.lua")
nut.util.include("cl_menu.lua")

function RECIPES:Get(name) 
	return self.recipes[name]
end

function RECIPES:GetAll()
	return RECIPES.recipes
end

function RECIPES:GetItem(item)
	local tblRecipe = self:Get(item)
	return tblRecipe.items
end

function RECIPES:GetResult(item)
	local tblRecipe = self:Get(item)
	return tblRecipe.result
end

function RECIPES:CanCraft(client, item)
	local tblRecipe = self:Get(item)

	if (!tblRecipe:CanCraft(client)) then
		return 1
	end

	return 0
end

local entityMeta = FindMetaTable("Entity")
function entityMeta:IsCraftingTable()
	return self:GetClass() == "nut_craftingtable"	
end

if (CLIENT) then return end -- A partir d'ici, tout se fait côté serveur

util.AddNetworkString("nut_CraftItem")
net.Receive("nut_CraftItem",function(length, client)
	local item = net.ReadString()
	local cancraft = RECIPES:CanCraft(client, item)
	local tblRecipe = RECIPES:Get(item)

	if (cancraft == 0) then
		tblRecipe:ProcessCraftItems(client)
	else
		client:notifyLocalized("Vous avez besoin de plus de matériaux pour fabriquer %s", tblRecipe.name)
	end
end)

function PLUGIN:saveCraftTables() -- On sauvegarde l'emplacement des tables de craft
	local data = {}

	for k, v in ipairs(ents.FindByClass("nut_craftingtable")) do
		data[#data + 1] = {v:GetPos(), v:GetAngles()}
	end

	self:setData(data)
end

function PLUGIN:shallSaveCraftTables()
	self:saveCraftTables()
end

function PLUGIN:LoadData() -- On charge l'emplacement des tables de craft à l'ouverture du serveur
	local data = self:getData() or {}

	for k, v in ipairs(data) do
		local entity = ents.Create("nut_craftingtable")
		entity:SetPos(v[1])
		entity:SetAngles(v[2])
		entity:Spawn()
	end
end