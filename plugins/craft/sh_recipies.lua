local RECIPE = {} -- Nom du craft
RECIPE.uid = "example" -- Identifiant unique au craft
RECIPE.name = "un crâne" -- Apparaît dans la phrase "Vous avez fabriqué [name]"
RECIPE.category = "Matériaux" -- Catégorie dans laquelle se trouve le craft
RECIPE.model = "models/Gibs/HGIBS.mdl" -- Icone à afficher pour le craft
RECIPE.desc = "Un morceau du squelette, à l'intérieur de la tête." -- Description du craft
RECIPE.items = {
	["bone"] = 1, -- Objets nécessaire à la réalisation du craft, quantité nécessaire
}
RECIPE.result = {
	["skull"] = 1, -- Objets résultant du craft, quantité produite
}
RECIPES:Register(RECIPE) -- On enregistre le craft dans la table de craft