CLASS.name = "un Aspirant CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 8
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_ASP)
end


CLASS_CWU_ASP = CLASS.index