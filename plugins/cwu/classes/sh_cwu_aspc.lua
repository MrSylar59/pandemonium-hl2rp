CLASS.name = "un Aspirant Confirmé CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 12
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_ASPC)
end


CLASS_CWU_ASPC = CLASS.index