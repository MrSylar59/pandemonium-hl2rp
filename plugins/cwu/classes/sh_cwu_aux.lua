CLASS.name = "une Auxiliaire CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 20
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_AUX)
end


CLASS_CWU_AUX = CLASS.index