CLASS.name = "une Auxiliaire Déléguée CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 28
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_AUXD)
end


CLASS_CWU_AUXD = CLASS.index