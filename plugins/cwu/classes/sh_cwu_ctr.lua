CLASS.name = "un Contrôleur CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 36
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_CTR)
end


CLASS_CWU_CTR = CLASS.index