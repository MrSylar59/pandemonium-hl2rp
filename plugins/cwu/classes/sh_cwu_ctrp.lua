CLASS.name = "un Contrôleur Principal CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 40
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_CTRP)
end


CLASS_CWU_CTRP = CLASS.index