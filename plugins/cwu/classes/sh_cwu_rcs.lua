CLASS.name = "le Recteur Sectoriel CWU"
CLASS.desc = "Au plus bas de la CWU."
CLASS.pay = 48
CLASS.faction = FACTION_CWU

function CLASS:onCanBe(client)
	return client:isCwuClass(CLASS_CWU_RCS)
end


CLASS_CWU_RCS = CLASS.index