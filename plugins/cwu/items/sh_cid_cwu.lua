ITEM.name = "Carte CWU"
ITEM.desc = "Une belle carte en plastique permettant l'identification des CWU."
ITEM.model = "models/dorado/tarjeta3.mdl"
ITEM.factions = {FACTION_CP, FACTION_ADMIN}
ITEM.functions.Assigner = {
	onRun = function(item)
		local data = {}
			data.start = item.player:EyePos()
			data.endpos = data.start + item.player:GetAimVector()*96
			data.filter = item.player
		local entity = util.TraceLine(data).Entity

		if (IsValid(entity) and entity:IsPlayer() and entity:Team() == FACTION_CWU) then
			local status, result = item:transfer(entity:getChar():getInv():getID())

			if (status) then
				item:setData("name", entity:Name())
				item:setData("id", math.random(10000, 99999))
				
				return true
			else
				item.player:notify(result)
			end
		end

		return false
	end,
	onCanRun = function(item)
		return item.player:isCombine()
	end
}

function ITEM:getDesc()
	local description = self.desc.."\nCette carte CWU est assignée à "..self:getData("name", "personne")..", #"..self:getData("id", "00000")..".\nCette carte possède un tampon prioritaire."
	
	return description
end