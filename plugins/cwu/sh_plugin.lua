PLUGIN.name = "CWU"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Permet de jouer les CWU"

local aspRank = "<ASP>"
local aspcRank = "<ASP/C>"
local auxRank = "<AUX>"
local auxdRank = "<AUX/D>"
local ctrRank = "<CTR>"
local ctrpRank = "<CTR/P>"
local rcsRank = "<RCS>"

function isCwuFaction(faction) 
	return faction == FACTION_CWU
end

local playerMeta = FindMetaTable("Player")

function playerMeta:isCwu() 
	return isCwuFaction(self:Team())
end

function playerMeta:getCwuRank() 
	for k, v in ipairs(team.GetPlayers(FACTION_CWU)) do
		local name = string.PatternSafe(v:Name())
		
		if (name:find(aspRank)) then
			return CLASS_CWU_ASP
		elseif (name:find(aspcRank)) then
			return CLASS_CWU_ASPC
		elseif (name:find(auxRank)) then
			return CLASS_CWU_AUX
		elseif (name:find(auxdRank)) then
			return CLASS_CWU_AUXD
		elseif (name:find(ctrRank)) then
			return CLASS_CWU_CTR
		elseif (name:find(ctrpRank)) then
			return CLASS_CWU_CTRP
		elseif (name:find(rcsRank)) then
			return CLASS_CWU_RCS
		end
	end
end

function playerMeta:isCwuClass(class)
	return self:getCwuRank() == class
end

if (SERVER) then
	function PLUGIN:OnCharVarChanged(character, key, oldValue, value) 
		if (key == "name" and IsValid(character:getPlayer())) then
			if (character:getPlayer():isCwu()) then
				character:setClass(character:getPlayer():getCwuRank())
				hook.Run("PlayerCwuRankChanged", character:getPlayer())
			end
		end
	end

	function PLUGIN:PostPlayerLoadout(client)
		if (client:isCwu()) then
			client:getChar():setClass(client:getCwuRank())
			hook.Run("PlayerCwuRankChanged", client)
		else
			client:SetBodygroup(1, 1)
			local groups = client:getChar():getData("groups", {})
				groups[1] = 1
			client:getChar():setData("groups", groups)
		end
	end

	function PLUGIN:PlayerCwuRankChanged(client) 
		if (client:isCwu()) then 
			if (client:isCwuClass(CLASS_CWU_RCS)) then
				client:SetBodygroup(1, 14)
				local groups = client:getChar():getData("groups", {})
					groups[1] = 14
				client:getChar():setData("groups", groups)
			elseif(client:isCwuClass(CLASS_CWU_CTR) or client:isCwuClass(CLASS_CWU_CTRP)) then
				client:SetBodygroup(1, 13)
				local groups = client:getChar():getData("groups", {})
					groups[1] = 13
				client:getChar():setData("groups", groups)
			else
				client:SetBodygroup(1, 12)
				local groups = client:getChar():getData("groups", {})
					groups[1] = 12
				client:getChar():setData("groups", groups)
			end
		end
	end
end


function isCwuElite(client)
	return client:getChar():getClass() == CLASS_CWU_CTR or client:getChar():getClass() == CLASS_CWU_CTRP or client:getChar():getClass() == CLASS_CWU_RCS
end

function promoteCwu(rank, target)
	targetName = target:Name():sub(target:Name():find(">")+2)
	target:getChar():setName(rank .. " " .. targetName)
end

nut.chat.register("broadcast", {
	color = Color(244, 146, 66),
	onCanSay = function(client)
		if (!isCwuElite(client) and !client:getChar():getClass() == CLASS_CWU_CTR) then
			client:notifyLocalized("notAllowed")
			return false
		end
	end,
	onChatAdd = function(speaker, text)
		chat.AddText(Color(244, 146, 66), L("%s diffuse : %s", speaker:getChar():getName(), text))
	end,
	prefix = {"/broadcast"}
})

nut.command.add("promotecwu", {
	syntax = "<string name>",
	onRun = function(client, arguments)
		local target = nut.command.findPlayer(client, table.concat(arguments, " "))

		if (!client:isCwu()) then
			return "Vous n'êtes pas un CWU !"
		elseif (!isCwuElite(client)) then
			return "Vous devez être au moins Contrôleur pour promouvoir un CWU !"
		elseif (!target:isCwu()) then
			return "Vous ne pouvez promouvoir que les CWU !"
		elseif (client == target) then
			return "Vous ne pouvez pas vous promouvoir vous-même !"
		elseif (IsValid(target) and target:getChar()) then
			if (target:isCwuClass(CLASS_CWU_ASP)) then
				promoteCwu("<ASP/C>", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu ".. target:Name() .." au rang d'Aspirant Confirmé !"
			elseif (target:isCwuClass(CLASS_CWU_ASPC)) then
				promoteCwu("<AUX>", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu ".. target:Name() .." au rang d'Auxiliaire !"
			elseif (target:isCwuClass(CLASS_CWU_AUX)) then
				promoteCwu("<AUX/D>", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu ".. target:Name() .." au rang d'Auxiliaire Déléguée !"
			elseif (target:isCwuClass(CLASS_CWU_AUXD)) then
				promoteCwu("<CTR>", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu ".. target:Name() .." au rang de Contrôleur !"
			elseif (target:isCwuClass(CLASS_CWU_CTR) and (client:isCwuClass(CLASS_CWU_CTRP) or client:isCwuClass(CLASS_CWU_RCS))) then
				promoteCwu("<CTR/P>", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu ".. target:Name() .." au rang de Contrôleur Principale !"
			elseif (target:isCwuClass(CLASS_CWU_CTRP) and client:isCwuClass(CLASS_CWU_RCS)) then
				promoteCwu("<RCS>", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu ".. target:Name() .." au rang de Recteur Sectoriel !"
			else
				return "Vous ne pouvez offrir une promotion supérieur à la votre !"
			end
		end
	end
})

nut.command.add("demotecwu", {
	syntax = "<string name>",
	onRun = function(client, arguments)
		local target = nut.command.findPlayer(client, table.concat(arguments, " "))

		if (!client:isCwu()) then
			return "Vous n'êtes pas un CWU !"
		elseif (!isCwuElite(client)) then
			return "Vous devez être au moins Contrôleur pour rétrograder un CWU !"
		elseif (!target:isCwu()) then
			return "Vous ne pouvez rétrograder que les CWU !"
		elseif (client == target) then
			return "Vous ne pouvez pas vous rétrograder vous-même !"
		elseif (IsValid(target) and target:getChar()) then
			if (target:isCwuClass(CLASS_CWU_ASP)) then
				return target:Name() .. " est déjà au rang minimal de la CWU !"
			elseif (target:isCwuClass(CLASS_CWU_ASPC)) then
				promoteCwu("<ASP>", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé ".. target:Name() .." au rang d'Aspirant !"
			elseif (target:isCwuClass(CLASS_CWU_AUX)) then
				promoteCwu("<ASP/C>", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé ".. target:Name() .." au rang d'Aspirant Confirmé !"
			elseif (target:isCwuClass(CLASS_CWU_AUXD)) then
				promoteCwu("<AUX>", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé ".. target:Name() .." au rang d'Auxiliaire !"
			elseif (target:isCwuClass(CLASS_CWU_CTR) and (client:isCwuClass(CLASS_CWU_CTRP) or client:isCwuClass(CLASS_CWU_RCS))) then
				promoteCwu("<AUX/D>", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé ".. target:Name() .." au rang de Auxiliaire Déléguée !"
			elseif (target:isCwuClass(CLASS_CWU_CTRP) and client:isCwuClass(CLASS_CWU_RCS)) then
				promoteCwu("<CTR>", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé ".. target:Name() .." au rang de Contrôleur !"
			else
				return "Vous ne pouvez rétrograder ni un supérieur ni un égal !"
			end
		end
	end
})