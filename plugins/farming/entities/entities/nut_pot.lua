AddCSLuaFile()

local PLUGIN = PLUGIN

ENT.Type = "anim"
ENT.PrintName = "Pot"
ENT.Category = "HL2 RP"
ENT.Author = "MrSylar59"
ENT.Spawnable = true
ENT.AdminOnly = true
ENT.PhysgunDisable = true

function ENT:SetupDataTables() 
	self:NetworkVar("Bool", 0, "Growing")
	self:NetworkVar("Bool", 1, "DoneGrowing")
end

function ENT:SpawnFunction(client, trace)
	local entity = ents.Create("nut_pot")
	entity:SetPos(trace.HitPos)
	entity:Spawn()
	entity:Activate()
	PLUGIN:savePots()
	PLUGIN:setTimeModifier(client:getChar():getAttrib("gar", 0)) -- La fonction item:getOwner() ne marche que dans l'inventaire...

	return entity
end

if (SERVER) then

	local grownPlant = nil

	local seed = {Model = "models/weapons/w_bugbait.mdl", Pos = Vector(0, 0, 10), Ang = Angle(0, 0, 0)}
	local plants = {
		["turnip"] = {Model = "models/tsbb/vegetables/turnip.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 2700}, -- = 45 min
		["sweet_potato"] = {Model = "models/tsbb/vegetables/sweet_potato.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 1800}, -- = 30 min
		["radish"] = {Model = "models/tsbb/vegetables/radish.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 1200}, -- = 20 min
		["parsnip"] = {Model = "models/tsbb/vegetables/parsnip.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 1800},
		["onion"] = {Model = "models/tsbb/vegetables/onion.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 1500}, -- = 25 min
		["lettuce"] = {Model = "models/tsbb/vegetables/lettuce.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 2700},
		["daikon"] = {Model = "models/tsbb/vegetables/daikon.mdl", Pos = Vector(0, 0, 6), Ang = Angle(-90, 0, 0), Time = 2100}, -- 35 min
		["cauliflower"] = {Model = "models/tsbb/vegetables/cauliflower.mdl", Pos = Vector(0, 0, 6), Ang = Angle(0, 0, 0), Time = 2700}
	}

	function attachModel(table)
		local parent = table.Parent
		local ang = parent:GetAngles()

		local child = ents.Create("prop_dynamic")
		parent:SetAngles(Angle(0, 0, 0))
		child:SetModel(table.Model or "models/weapons/w_bugbait.mdl")
		child:SetPos(parent:GetPos() + table.Pos)
		child:SetAngles(parent:GetAngles() + table.Ang)
		child:SetParent(parent)
		parent:DeleteOnRemove(child) -- On fait en sorte que si l'objet parent est delete, l'objet enfant le soit aussi
		child:Spawn()
		parent:SetAngles(ang)
	end

	function ENT:detachModels() 
		for k, v in pairs(self:GetChildren()) do
			v:Remove()
		end
	end

	function ENT:Initialize() 
		self:SetModel("models/nater/weedplant_pot_dirt.mdl")
		self:SetSolid(SOLID_VPHYSICS)
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		self:SetUseType(SIMPLE_USE)
		self:SetGrowing(false)
		self:SetDoneGrowing(true)

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:Wake()
		end
	end

	function ENT:Use(activator) 
		if (!self:GetGrowing()) then
			activator:getChar():getInv():add("pot")
			self:Remove()
		elseif (self:GetGrowing() and !self:GetDoneGrowing()) then
			activator:notifyLocalized("Vous n'avez rien récolté, les plantes n'étaient pas prêtes.")
			self:detachModels()
			self:SetGrowing(false)
			self:SetDoneGrowing(false)
		elseif (self:GetGrowing() and self:GetDoneGrowing()) then
			activator:getChar():getInv():add(grownPlant)
			activator:getChar():updateAttrib("gar", 0.2)
			self:detachModels()
			self:SetGrowing(false)
			self:SetDoneGrowing(false)
		end
	end

	function ENT:Touch(ent)
		if (ent:GetClass() == "nut_item") then
			local item = ent:getItemTable()
			if (item.uniqueID:sub(0, 5) == "seed_" and !self:GetGrowing()) then
				local plant = item.uniqueID:sub(6)

				ent:Remove()
				seed.Parent = self
				attachModel(seed)
				self:SetGrowing(true)
				grownPlant = plant
				self:growPlant(plant, ent:GetCreationID())
			end
		end
	end

	function ENT:growPlant(plant, uid)
		timer.Create("nutFarm_" .. uid, plants[plant].Time - (PLUGIN:getTimeModifier() or 0)*GAR_MULTIPLIER, 1, function()
			if (self:GetGrowing(true)) then
				self:detachModels()
				plants[plant].Parent = self
				attachModel(plants[plant])
				self:SetDoneGrowing(true)
			end
		end)
	end

	function ENT:OnRemove() 
		if (!nut.shuttingDown and !nut.cleaningUp) then
			PLUGIN:savePots()
		end
	end

end