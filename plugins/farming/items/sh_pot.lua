ITEM.name = "Pot"
ITEM.desc = "Un poetit pot orange contenant de la terre."
ITEM.model = "models/nater/weedplant_pot_dirt.mdl"
ITEM.category = "Ferme"
ITEM.permit = "seed"
ITEM.functions.Utiliser = {
	icon = "icon16/add.png",
	onRun = function (item)
		local data = {}
		data.start = item.player:GetShootPos()
		data.endpos = data.start + item.player:GetAimVector()*128
		data.filter = item.player

		scripted_ents.Get("nut_pot"):SpawnFunction(item.player, util.TraceLine(data))
	end
}