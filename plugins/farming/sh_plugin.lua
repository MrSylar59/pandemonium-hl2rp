PLUGIN.name = "Ferme Alimentaire"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute de quoi faire pousser de la nouriture fraîche."

if (SERVER) then

	GAR_MULTIPLIER = 9 -- Permet de gagner 15 minutes sur un temps de pousse si la compétence "Jardinage" est à 100
	timeMultiplier = nil

	function PLUGIN:savePots() 
		local data = {}

		for k, v in ipairs(ents.FindByClass("nut_pot")) do
			data[#data + 1] = {v:GetPos(), v:GetAngles()}
		end

		self:setData(data)
	end

	function PLUGIN:LoadData() 
		local data = self:getData() or {}

		for k, v in ipairs(data) do
			local entity = ents.Create("nut_pot")
			entity:SetPos(v[1])
			entity:SetAngles(v[2])
			entity:Spawn()
		end
	end

	function PLUGIN:setTimeModifier(time)
		timeMultiplier = time
	end

	function PLUGIN:getTimeModifier() 
		return timeMultiplier
	end

end