PLUGIN.name = "Lampe Torche"
PLUGIN.author = "Chessnut"
PLUGIN.desc = "Ajoute des lampes torches utilisables en jeu."

function PLUGIN:PlayerSwitchFlashlight(client, state)
	local character = client:getChar()

	if (!character or !character:getInv()) then
		return false
	end

	if (character:getInv():hasItem("flashlight")) then
		return true
	end
end