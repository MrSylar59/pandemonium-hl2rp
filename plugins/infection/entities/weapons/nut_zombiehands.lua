AddCSLuaFile()

if (CLIENT) then
	SWEP.PrintName = "Mains"
	SWEP.Slot = 0
	SWEP.SlotPos = 1
	SWEP.DrawAmmo = false
	SWEP.DrawCrosshair = false
end

SWEP.Author = "MrSylar59"
SWEP.Instructions = "Clic Gauche: Frapper\nClic Droit: Pousser les objets."
SWEP.Purpose = "Frapper tout ce qui bouge."
SWEP.Drop = false

SWEP.ViewModelFOV = 45
SWEP.ViewModelFlip = false
SWEP.AnimPrefix = "rpg"

SWEP.ViewTranslation = 4

SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = ""
SWEP.Primary.Damage = 10
SWEP.Primary.Delay = 1.6

SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = ""

SWEP.ViewModel = Model("models/weapons/c_arms_cstrike.mdl")
SWEP.WorldModel = ""

SWEP.UseHands = false

SWEP.IsAlwaysRaised = true
SWEP.HoldType = "fist"

local Sequences = {"attackA", "attackB", "attackC", "attackD", "attackE", "attackF"}

function SWEP:PreDrawViewModel(viewModel, weapon, client) 
	local hands = player_manager.TranslatePlayerHands(player_manager.TranslateToPlayerModelName(client:GetModel()))

	if (hands and hands.model) then
		viewModel:SetModel(hands.model)
		viewModel:SetSkin(hands.skin)
		viewModel:SetBodyGroups(hands.body)
	end
end

ACT_VM_FISTS_DRAW = 3
ACT_VM_FISTS_HOLSTER = 2

function SWEP:Deploy() 
	if (!IsValid(self.Owner)) then
		return
	end

	local viewModel = self.Owner:GetViewModel()

	if (IsValid(viewModel)) then
		viewModel:SetPlaybackRate(1)
		viewModel:ResetSequence(ACT_VM_FISTS_DRAW)
	end

	return true
end

function SWEP:Holster() 
	if (!IsValid(self.Owner)) then
		return
	end

	local viewModel = self.Owner:GetViewModel()

	if (IsValid(viewModel)) then
		viewModel:SetPlaybackRate(1)
		viewModel:ResetSequence(ACT_VM_FISTS_HOLSTER)
	end

	return true
end

function SWEP:Think() 
	if (CLIENT) then
		if (self.Owner) then
			local viewModel = self.Owner:GetViewModel()

			if (IsValid(viewModel)) then
				viewModel:SetPlaybackRate(1)
			end
		end
	end
end

function SWEP:Precache() 
	util.PrecacheSound("npc/zombie/zo_attack1.wav")
	util.PrecacheSound("npc/zombie/zo_attack2.wav")
	util.PrecacheSound("npc/zombie/claw_strike1.wav")
	util.PrecacheSound("npc/zombie/claw_strike2.wav")
	util.PrecacheSound("npc/zombie/claw_strike3.wav")

	util.PrecacheSound("physics/wood/wood_crate_impact_hard1.wav")
	util.PrecacheSound("physics/wood/wood_crate_impact_hard2.wav")
	util.PrecacheSound("physics/wood/wood_crate_impact_hard3.wav")
	util.PrecacheSound("physics/wood/wood_crate_impact_hard4.wav")
	util.PrecacheSound("physics/wood/wood_crate_impact_hard5.wav")
end

function SWEP:Initialize() 
	self:SetHoldType(self.HoldType)
	self.LastHand = 0
end

function SWEP:DoAttackAnimation() 
	self.LastHand = math.abs(1 - self.LastHand)

	local sequence = 4 + self.LastHand
	local viewModel = self.Owner:GetViewModel()

	if (IsValid(viewModel)) then
		viewModel:SetPlaybackRate(0.05)
		viewModel:SetSequence(sequence)
	end
end

function SWEP:PrimaryAttack() 
	if (!IsFirstTimePredicted()) then
		return
	end

	self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	self:SetNextSecondaryFire(CurTime() + self.Primary.Delay)

	if (hook.Run("CanPlayerThrowPunch", self.Owner) == false) then
		return
	end

	local staminaUse = 1.5 * nut.config.get("punchStamina")

	if (staminaUse > 0) then
		local value = self.Owner:getLocalVar("stm", 0) - staminaUse

		if (value < 0) then
			return
		elseif (SERVER) then
			self.Owner:setLocalVar("stm", value)
		end
	end

	if (SERVER) then
		self.Owner:EmitSound("npc/zombie/zo_attack"..math.random(1, 2)..".wav")
		self.Owner:forceSequence(table.Random(Sequences), nil, nil, false)
	end

	local damage = self.Primary.Damage

	self:DoAttackAnimation()
	self.Owner:ViewPunch(Angle(self.LastHand + 2, self.LastHand + 5, 0.125))

	timer.Simple(1.6, function() 
		if (IsValid(self) and IsValid(self.Owner)) then
			local damage = self.Primary.Damage
			local context = {damage = damage}
			local result = hook.Run("PlayerGetFistDamage", self.Owner, damage, context)

			if (result != nil) then
				damage = result
			else
				damage = context.damage
			end

			self.Owner:LagCompensation(true)
				local data = {}
					data.start = self.Owner:GetShootPos()
					data.endpos = data.start + self.Owner:GetAimVector()*96
					data.filter = self.Owner
				local trace = util.TraceLine(data)

				if (SERVER and trace.Hit) then
					local entity = trace.Entity

					if (IsValid(entity)) then
						local damageInfo = DamageInfo()
							damageInfo:SetAttacker(self.Owner)
							damageInfo:SetInflictor(self)
							damageInfo:SetDamage(damage)
							damageInfo:SetDamageType(DMG_SLASH)
							damageInfo:SetDamagePosition(trace.HitPos)
							damageInfo:SetDamageForce(self.Owner:GetAimVector()*10000)
						entity:DispatchTraceAttack(damageInfo, data.start, data.endpos)

						self.Owner:EmitSound("npc/zombie/claw_strike"..math.random(1, 3)..".wav")
					end
				end

				hook.Run("PlayerThrowPunch", self.Owner, trace.Hit)
			self.Owner:LagCompensation(false)
		end
	end)
end

function SWEP:SecondaryAttack()
	self.Owner:LagCompensation(true)
		local data = {}
			data.start = self.Owner:GetShootPos()
			data.endpos = data.start + self.Owner:GetAimVector()*72
			data.filter = self.Owner
			data.mins = Vector(-8, -8, -30)
			data.maxs = Vector(8, 8, 30)
		local trace = util.TraceHull(data)
		local entity = trace.Entity
	self.Owner:LagCompensation(false)

	self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
	self:SetNextSecondaryFire(CurTime() + self.Primary.Delay)

	local staminaUse = 1.5 * nut.config.get("punchStamina")

	if (staminaUse > 0) then
		local value = self.Owner:getLocalVar("stm", 0) - staminaUse

		if (value < 0) then
			return
		elseif (SERVER) then
			self.Owner:setLocalVar("stm", value)
		end
	end

	if (SERVER) then
		self.Owner:EmitSound("npc/zombie/zo_attack"..math.random(1, 2)..".wav")
		self.Owner:forceSequence(table.Random(Sequences), nil, nil, false)
	end

	self:DoAttackAnimation()

	if (SERVER and IsValid(entity)) then
		local pushed

		timer.Simple(1.4, function() 
			if (entity:isDoor()) then
				if (hook.Run("PlayerCanKnock", self.Owner, entity) == false) then
					return
				end

				self.Owner:ViewPunch(Angle(-1.3, 1.8, 0))
				self.Owner:EmitSound("physics/wood/wood_crate_impact_hard"..math.random(1, 4)..".wav")

				self:SetNextSecondaryFire(CurTime() + 1.6)
				self:SetNextPrimaryFire(CurTime() + 1.6)
			elseif (entity:IsPlayer()) then
				local direction = self.Owner:GetAimVector() * (400 + (self.Owner:getChar():getAttrib("str", 0) * 3))
				direction.z = 0

				entity:SetVelocity(direction)
				entity:TakeDamage(5, self.Owner, self)

				pushed = true
			else
				local physObj = entity:GetPhysicsObject()

				if (IsValid(physObj)) then
					physObj:SetVelocity(self.Owner:GetAimVector() * 500)
				end

				pushed = true
			end

			if (pushed) then
				self:SetNextSecondaryFire(CurTime() + 1.6)
				self:SetNextPrimaryFire(CurTime() + 1.6)
				self.Owner:EmitSound("npc/zombie/claw_strike"..math.random(1, 3)..".wav")
			end
		end)
	end
end