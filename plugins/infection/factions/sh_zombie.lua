FACTION.name = "Zombie"
FACTION.desc = "Les âmes perdues, les citoyens infectées."
FACTION.color = Color(120, 70, 0)
FACTION.isDefault = false
FACTION.models = {"models/zombie/classic.mdl"}
FACTION.pay = 0

FACTION_ZOMBIE = FACTION.index