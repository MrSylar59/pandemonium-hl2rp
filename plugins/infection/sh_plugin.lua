PLUGIN.name = "Infection"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute un système d'infection permettant aux joueurs de se changer en zombies sous certaines conditions"

local function isZombieFaction(faction) 
	return faction == FACTION_ZOMBIE
end

local playerMeta = FindMetaTable("Player")

function playerMeta:isZombie() 
	return isZombieFaction(self:Team())
end

function PLUGIN:PlayerFootstep(client, pos, foot, sound, volume, filter) 
	if (client:isZombie()) then
		local sounds = self.footStep

		if (foot == 1) then
			client:EmitSound("npc/zombie/foot"..math.random(1, 3)..".wav")
		else
			client:EmitSound("npc/zombie/foot_slide"..math.random(1, 3)..".wav")
		end

		return true
	end
end

if (SERVER) then

	local function GetRandomChance(client)
		local stm = client:getLocalVar("stm", 0)
		local health = client:Health()

		return (math.random(200, 400) - (stm + health))
	end

	local function IsNpcZombie(attacker)
		if (!attacker:IsNPC()) then return end

		if (attacker:GetClass():find("zombie") or attacker:GetClass():find("headcrab")) then
			return true
		end

		return false
	end

	local function ChangeZombieRelation(client, rel) 
		local zombies = ents.FindByClass("npc_*")

		for _, zombie in pairs(zombies) do
			if (!zombie:IsNPC()) then return end

			if (IsNpcZombie(zombie)) then
				zombie:AddEntityRelationship(client, rel, 99)
			end
		end
	end

	function playerMeta:DoInfection() 
		local faction = nut.faction.teams["zombie"]

		if (self.bleeding) then
			self.bleeding = nil
		end

		self:EmitSound("ambient/creatures/town_zombie_call1.wav")
		self:notifyLocalized("Votre transformation est complète, vous êtes un Zombie.")

		if (faction) then
			self:getChar().vars.faction = faction.uniqueID
			self:getChar():setFaction(faction.index)
			self:setNetVar("infected", false)
			self:getChar():setModel("models/zombie/classic.mdl")
			self:SetupHands()
			self:SetWalkSpeed(self:GetWalkSpeed() / 2)
			self:SetRunSpeed(self:GetRunSpeed() / 2)
			self:StripWeapons()
			self:Give("nut_zombiehands")

			for k, v in pairs(self:getChar():getInv():getItems()) do
				if (v.isWeapon) then
					v:setData("ammo", nil)
					v:setData("equip", nil)
					nut.item.spawn(v.uniqueID, self:GetShootPos(), function() 
						v:remove()
					end, Angle(), v.data)
				end
			end


			self:SetBodygroup(1, 1)
			local groups = self:getChar():getData("groups", {})
				groups[1] = 1
			self:getChar():setData("groups", groups)

			ChangeZombieRelation(self, D_NU)
		else
			print("[Infection Plugin] - Erreur : La faction zombie n'a pas été trouvée !")
		end
	end

	function playerMeta:Infect() 

		self:notifyLocalized("Vous vous sentez fortement nauséeux, vos forces s'amenuisent.")
		self:setNetVar("infected", true)

		timer.Create("nut_infect_"..self:SteamID64(), 360, 1, function()
			self:DoInfection()
		end)
	end

	function PLUGIN:EntityTakeDamage(ent, dmg) 
		if (ent:IsPlayer()) then
			local attacker = dmg:GetAttacker()
			local aIsZombie = IsNpcZombie(attacker)
			local randomChance = GetRandomChance(ent)

			if ((IsValid(ent) or attacker:isZombie()) and ent:IsPlayer() and ent:GetMoveType() != MOVETYPE_NOCLIP) then
				if (!ent:isZombie()) then
					if (aIsZombie and randomChance >= 200) then
						ent:Infect()
					end
				end
			end
		end
	end

	function PLUGIN:GetPlayerPainSound(client)
		if (client:isZombie()) then
			return "npc/zombie/zombie_pain"..math.random(1, 6)..".wav"
		end
	end

	function PLUGIN:GetPlayerDeathSound(client)
		if (client:isZombie()) then
			return "npc/zombie/zombie_die"..math.random(1, 3)..".wav"
		end
	end

	function PLUGIN:PlayerSay(client, text, teamChat)
		if (client:isZombie()) then
			if (text:find("/w ") or text:find("/y ")) then 
				client:EmitSound("npc/zombie/zombie_voice_idle".. math.random(1, 14) ..".wav", math.random(65, 80), math.random(90, 110))
			elseif (text:find("/")) then

			else
				client:EmitSound("npc/zombie/zombie_voice_idle".. math.random(1, 14) ..".wav", math.random(65, 80), math.random(90, 110))
			end
		end
	end

	function PLUGIN:PostPlayerLoadout(client) 
		if (client:isZombie()) then
			client:SetWalkSpeed(client:GetWalkSpeed() / 2)
			client:SetRunSpeed(client:GetRunSpeed() / 2)
			ChangeZombieRelation(client, D_NU)
		else
			ChangeZombieRelation(client, D_HT)
		end

		timer.Simple(0.2, function() 
			if (client:isZombie()) then
				client:StripWeapons()
				client:Give("nut_zombiehands")
			end
		end)
	end

	function PLUGIN:CanPlayerTakeItem(client, item) 
		if (client:isZombie()) then
			return false
		end
	end

	function PLUGIN:CanPlayerDropItem(client, item) 
		if (client:isZombie()) then
			return false
		end
	end

	function PLUGIN:PlayerSpawn(client) 
		local infected = self:getData() or {}

		client:setNetVar("infected", false)

		for k, v in pairs(infected) do
			if (k == client:GetName() and v.steamID == client:SteamID64()) then
				if (!client:isZombie()) then
					client:setNetVar("infected", true)
					timer.Create("nut_infect_"..client:SteamID64(), 360, function() 
						client:DoInfection()
					end)
				else
					self:SaveInfectedStatus(client)
				end
			end
		end
	end

	function PLUGIN:CharacterLoaded(id)
		local char = nut.char.loaded[id]
		local client = char:getPlayer()

		self:SaveInfectedStatus(client)
	end

	function PLUGIN:PlayerDisconnected(client)
		self:SaveInfectedStatus(client)
	end

	function PLUGIN:SaveInfectedStatus(client)
		local infected = self:getData() or {}

		if (client:getNetVar("infected", false) == true) then
			infected[client:GetName()] = {steamID = client:SteamID64()}
		end

		if (client:isZombie() and infected[client:GetName()]) then
			infected[client:GetName()] = nil
		end

		if (timer.Exists("nut_infect_"..client:SteamID64())) then
			timer.Remove("nut_infect_"..client:SteamID64())
		end

		self:setData()
	end

	timer.Create("nut_friendlyZombies", 120, 0, function() 
		local players = player.GetAll()

		for _, player in pairs(players) do
			if (player:isZombie()) then
				ChangeZombieRelation(player, D_NU)
			else
				ChangeZombieRelation(player, D_HT)
			end
		end
	end)

else

	local color = {
		[ "$pp_colour_addr" ] = 0,
		[ "$pp_colour_addg" ] = 0,
		[ "$pp_colour_addb" ] = 0,
		[ "$pp_colour_brightness" ] = -0.01,
		[ "$pp_colour_contrast" ] = 1.35,
		[ "$pp_colour_colour" ] = 0.65,
		[ "$pp_colour_mulr" ] = 144,
		[ "$pp_colour_mulg" ] = 0,
		[ "$pp_colour_mulb" ] = 0
	}

	function PLUGIN:OnChatReceived(client, chatType, text, anonymous)
		local randomMoans = {"Gaaaah...", "Braa...", "Gnn.. Braaff...", "Nffraa.. Bfggh.", "Mmrrah...", "Aaffr fgh... Hal..rrr", "Jaa.. ssfff....", "Ghh.."}
		local class = nut.chat.classes[chatType]

		if (client:isZombie() and class and class.filter == "ic" and chatType != "dispatch") then
			return randomMoans[math.random(1, 7)]
		end
	end

	function PLUGIN:RenderScreenspaceEffects() 
		if (LocalPlayer():getNetVar("infected", false) == true and !IsValid(nut.gui.char)) then
			DrawMotionBlur(0.20, 0.65, 0)
		end
		if (LocalPlayer():isZombie() and !IsValid(nut.gui.char)) then
			DrawColorModify(color)
			DrawSharpen(10, 0.25)
		end
	end

end