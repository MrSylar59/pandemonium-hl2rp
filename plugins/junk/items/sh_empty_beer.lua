ITEM.name = "Bouteille de Bière Vide"
ITEM.desc = "Une bouteille de bière vidée de son contenue.\nUne odeur particulière s'en dégage."
ITEM.model = "models/bioshockinfinite/hext_bottle_lager.mdl"
ITEM.category = "Déchêt"
ITEM.permit = "none"