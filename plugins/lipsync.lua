PLUGIN.name = "Syncro Labiale"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Permet de faire bouger les lèvres d'un joueur quand il écrit quelque chose."

if (SERVER) then

	local sentences = {}
	sentences["short"] = {
		"hi01",
		"hi02",
		"ok01",
		"ok02",
		"no01",
		"no02",
		"ow01",
		"ow02"
	}
	sentences["medium"] = {
		"question20",
		"question21",
		"question25",
		"question27",
		"question29",
		"question30",
		"question01",
		"question07",
		"question08",
		"question12",
		"question13",
		"question15",
		"question18",
		"question19"
	}
	sentences["long"] = {
		"question02",
		"question04",
		"question06",
		"question09",
		"question10",
		"question11",
		"question14",
		"gordead_ques15",
		"abouttime02"
	}

	local playerMeta = FindMetaTable("Player")

	function playerMeta:PlayICSentence(len)
		if (len < 10) then
			self:EmitSound("vo/npc/male01/"..table.Random(sentences["short"])..".wav", 1, 100, 0)
		elseif (len < 30) then
			self:EmitSound("vo/npc/male01/"..table.Random(sentences["medium"])..".wav", 1, 100, 0)
		else
			self:EmitSound("vo/npc/male01/"..table.Random(sentences["long"])..".wav", 1, 100, 0)
		end
	end

	function PLUGIN:PlayerMessageSend(client, chatType, text, anon, receivers)
		if (!client:isCombine() and !client:isZombie()) then
			if (chatType == "ic" or chatType == "y" or chatType == "w" or chatType == "radio") then
				client:PlayICSentence(text:len())
			end
		end
	end

end