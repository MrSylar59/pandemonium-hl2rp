PLUGIN.name = "Plus d'attributs"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute divers attributs supplémentaires."

-- On s'assure d'être côté serveur pour modifier la force d'un saut
if (SERVER) then
	-- Le multiplieur de saut en focntion des points dans l'attribut
	local ACRO_MULTIPLIER = 0.5 -- Valeur à tester

	function PLUGIN:KeyPress(client, key) 
		-- Si le joueur appuie sur sa touche de saut et qu'il n'est pas en noclip (coucou les admins ;3 )
		if (key == IN_JUMP and client:GetMoveType() != MOVETYPE_NOCLIP and client:OnGround()) then
			-- Force à laquelle un joueur "incompétent" en acrobatie va sauter
			local playerJumpForce = 150

			if (client:getLocalVar("stm", 0) > 10 and !client.nutBreathing) then
			    -- On modifie la force de saut du joueur qui tente de sauter en fonction de son acrobatie
				client:SetJumpPower(playerJumpForce + client:getChar():getAttrib("acro", 0) * ACRO_MULTIPLIER)
			    -- Plus le joueur saute, plus son acrobatie augmente (250 sauts = +1 point d'acrobatie)
				client:getChar():updateAttrib("acro", 0.004)
			    -- Plus le joueur saute, plus sa vigueur diminue
				client:restoreStamina(-10)
			else
				-- Si le joueur n'a pas assez de vigueur, il reste cloué au sol
				client:SetJumpPower(0)
				-- Et il est essoufflé
				client.nutBreathing = true
				client:SetRunSpeed(nut.config.get("walkSpeed"))
				hook.Run("PlayerStaminaLost", client)
			end
		end
	end
end