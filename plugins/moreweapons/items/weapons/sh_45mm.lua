ITEM.name = "45mm"
ITEM.desc = "Un pistolet semi-automatique utilisé par la Protection Civile."
ITEM.model = "models/tnb/weapons/w_45.mdl"
ITEM.class = "weapon_45mm"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(0.33879372477531, 270.15808105469, 0),
	fov	= 5.0470897275697,
	pos	= Vector(0, 200, -1)
}