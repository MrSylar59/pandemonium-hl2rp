ITEM.name = "AK-74"
ITEM.desc = "Une version améliorée d'un fusil d'assaut russe très célèbre."
ITEM.model = "models/tnb/weapons/w_ak74.mdl"
ITEM.class = "weapon_ak74"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 350, -1)
}