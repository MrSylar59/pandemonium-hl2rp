ITEM.name = "Batte"
ITEM.desc = "Un morceau de bois rigide, autrefois utilisé pour faire du sport."
ITEM.model = "models/tnb/weapons/w_bat.mdl"
ITEM.class = "weapon_bat"
ITEM.weaponCategory = "melee"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.23955784738064, 270.44906616211, 0),
	fov	= 10.780103254469,
	pos	= Vector(0, 200, 0)
}