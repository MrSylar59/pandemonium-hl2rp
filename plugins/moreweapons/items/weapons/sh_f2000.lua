ITEM.name = "F2000"
ITEM.desc = "Un fusil d'assaut sur-puissant utilisé par les forces de l'Overwatch."
ITEM.model = "models/tnb/weapons/w_f2000.mdl"
ITEM.class = "weapon_f2000"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 350, -1)
}