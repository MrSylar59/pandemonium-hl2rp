ITEM.name = "Famas"
ITEM.desc = "Un fusil d'assaut léger et polyvalent utilisé par la Protection Civile."
ITEM.model = "models/tnb/weapons/w_famas.mdl"
ITEM.class = "weapon_famas"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 350, -1)
}