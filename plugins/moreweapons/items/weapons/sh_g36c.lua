ITEM.name = "G36C"
ITEM.desc = "Un fusil d'assaut lourd capable de tirer à cadence soutenue."
ITEM.model = "models/tnb/weapons/w_g36c.mdl"
ITEM.class = "weapon_g36c"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 350, -1)
}