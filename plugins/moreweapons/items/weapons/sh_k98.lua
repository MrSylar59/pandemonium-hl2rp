ITEM.name = "K-98"
ITEM.desc = "Un fusil de précision un peu moins performant que ceux du Cartel."
ITEM.model = "models/tnb/weapons/w_k98.mdl"
ITEM.class = "weapon_k98"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 350, -1)
}