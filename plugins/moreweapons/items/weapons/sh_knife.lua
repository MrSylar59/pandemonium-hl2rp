ITEM.name = "Couteau de fortune"
ITEM.desc = "On dirait un vieux tournevis aiguisé... Vous doutez de son efficacité."
ITEM.model = "models/tnb/weapons/w_shank.mdl"
ITEM.class = "weapon_shank"
ITEM.weaponCategory = "melee"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.23955784738064, 270.44906616211, 0),
	fov	= 10.780103254469,
	pos	= Vector(0, 200, 0)
}