ITEM.name = "KS-23"
ITEM.desc = "Un fusil à pompe extrêmement puissant à courte portée."
ITEM.class = "weapon_ks23"
ITEM.weaponCategory = "primary"
ITEM.model = "models/tnb/weapons/w_ks23.mdl"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
    pos = Vector(0, 200, 1),
    ang = Angle(0, 270, 0),
    fov = 10
}