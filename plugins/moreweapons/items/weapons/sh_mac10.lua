ITEM.name = "MAC-10"
ITEM.desc = "Un pistolet-mitrailleur américain de bonne manufacture."
ITEM.model = "models/tnb/weapons/w_mac10.mdl"
ITEM.class = "weapon_mac10"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(0.33879372477531, 270.15808105469, 0),
	fov	= 5.0470897275697,
	pos	= Vector(0, 200, -1)
}