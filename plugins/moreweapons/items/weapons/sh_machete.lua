ITEM.name = "Machette"
ITEM.desc = "Un bout de métal tranchant bien qu'attaqué par la rouille."
ITEM.model = "models/tnb/weapons/w_machete.mdl"
ITEM.class = "weapon_machete"
ITEM.weaponCategory = "melee"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(-0.23955784738064, 270.44906616211, 0),
	fov	= 10.780103254469,
	pos	= Vector(0, 200, 0)
}