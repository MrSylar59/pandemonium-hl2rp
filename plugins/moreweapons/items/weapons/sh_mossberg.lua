ITEM.name = "Mossberg"
ITEM.desc = "Un fusil à pompe précis, efficasse à courte et moyenne portée utilisé par la Protection Civile."
ITEM.class = "weapon_mossberg"
ITEM.weaponCategory = "primary"
ITEM.model = "models/tnb/weapons/w_mossberg.mdl"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
    pos = Vector(0, 200, 1),
    ang = Angle(0, 270, 0),
    fov = 10
}