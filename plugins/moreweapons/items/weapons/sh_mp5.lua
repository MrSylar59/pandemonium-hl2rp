ITEM.name = "MP5"
ITEM.desc = "Un fusil d'assaut puissant et automatique utilisé par la Protection Civile."
ITEM.model = "models/tnb/weapons/w_mp5.mdl"
ITEM.class = "weapon_mp5"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 200, -1)
}