ITEM.name = "PPSH"
ITEM.desc = "Un fusil-mitrailleur russe. Il possède une grande contenance."
ITEM.model = "models/tnb/weapons/w_ppsh.mdl"
ITEM.class = "weapon_ppsh"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.iconCam = {
	ang	= Angle(-0.020070368424058, 270.40155029297, 0),
	fov	= 7.2253324508038,
	pos	= Vector(0, 300, -1)
}