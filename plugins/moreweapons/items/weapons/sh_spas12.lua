ITEM.name = "SPAS-12"
ITEM.desc = "Un fusil à pompe polyvalent utilisé par la Protection Civile."
ITEM.class = "weapon_spas12"
ITEM.weaponCategory = "primary"
ITEM.model = "models/tnb/weapons/w_spas12.mdl"
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
    pos = Vector(0, 200, 1),
    ang = Angle(0, 270, 0),
    fov = 10
}