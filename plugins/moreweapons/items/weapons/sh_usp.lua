ITEM.name = "USP"
ITEM.desc = "Un pistolet semi-automatique très puissant mais bruyant."
ITEM.model = "models/tnb/weapons/w_usp.mdl"
ITEM.class = "weapon_usp"
ITEM.weaponCategory = "sidearm"
ITEM.width = 2
ITEM.height = 1
ITEM.iconCam = {
	ang	= Angle(0.33879372477531, 270.15808105469, 0),
	fov	= 5.0470897275697,
	pos	= Vector(0, 200, -1)
}