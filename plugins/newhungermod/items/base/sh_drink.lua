ITEM.name = "Boisson"
ITEM.desc = "Une boisson."
ITEM.category = "Consommable"
ITEM.model = "models/bioshockinfinite/hext_apple.mdl"
ITEM.healthResore = 0
ITEM.staminaRestore = 0
ITEM.acro = 0
ITEM.endu = 0
ITEM.stre = 0
ITEM.stam = 0
ITEM.permit = "food"
ITEM.price = 0
ITEM.items = {}

ITEM.functions.Boire = {
	icon = "icon16/cup.png", 
	sound = "npc/barnacle/barnacle_gulp2.wav",
	onRun = function(item) 
		local client = item.player
		local char = client:getChar()

		client:SetHealth(math.min(client:Health() + item.healthRestore, 100))
		client:setLocalVar("stm", math.min(client:getLocalVar("stm", 100) + item.staminaRestore, 100))

		if (char:getAttrib("acro", 1) < 100) then
			char:updateAttrib("acro", item.acro)
		end

		if (char:getAttrib("end", 1) < 100) then		
			char:updateAttrib("end", item.endu)
		end

		if (char:getAttrib("str", 1) < 100) then
			char:updateAttrib("str", item.stre)
		end	
		
		if (char:getAttrib("stm", 1) < 100) then
			char:updateAttrib("stm", item.stam)
		end

		timer.Simple(0, function()
			for k, v in pairs(item.items) do
				if (IsValid(client) and char and !char:getInv():add(v)) then
					nut.item.spawn(v, position)
				end
			end
		end)
	end
}