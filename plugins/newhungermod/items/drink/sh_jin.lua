ITEM.name = "Gin"
ITEM.desc = "Une grosse bouteille d'une liqueur à base d'éthanol avec le logo de l'Union."
ITEM.model = "models/bioshockinfinite/jin_bottle.mdl"
ITEM.items = {"empty_jin"}
ITEM.healthRestore = -10
ITEM.staminaRestore = 30
ITEM.acro = -1
ITEM.endu = 0
ITEM.stre = 1
ITEM.stam = 1.5
ITEM.price = 50