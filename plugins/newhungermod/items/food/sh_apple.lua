ITEM.name = "Pomme"
ITEM.desc = "Une pomme légèrement déformée avec le logo de l'Union."
ITEM.model = "models/bioshockinfinite/hext_apple.mdl"
ITEM.healthRestore = 10
ITEM.staminaRestore = 5
ITEM.acro = 1
ITEM.endu = 0.5
ITEM.stre = 0
ITEM.stam = 0.5
ITEM.price = 25