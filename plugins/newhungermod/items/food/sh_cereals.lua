ITEM.name = "Céréales"
ITEM.desc = "Un paquet de céréales avec le logo de l'Union."
ITEM.model = "models/bioshockinfinite/hext_cereal_box_cornflakes.mdl"
ITEM.items = {"empty_cereals"}
ITEM.healthRestore = 25
ITEM.staminaRestore = 35
ITEM.acro = 0
ITEM.endu = 1
ITEM.stre = 0.5
ITEM.stam = 0.5
ITEM.price = 35