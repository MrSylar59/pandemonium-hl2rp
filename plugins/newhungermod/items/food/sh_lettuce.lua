ITEM.name = "Salade"
ITEM.desc = "Quelques feuilles de salade, ça se mange sans fin."
ITEM.model = "models/tsbb/vegetables/lettuce.mdl"
ITEM.healthRestore = 5
ITEM.staminaRestore = 5
ITEM.acro = 0.5
ITEM.endu = 0.25
ITEM.stre = 0.25
ITEM.stam = 0.25
ITEM.permit = "none"