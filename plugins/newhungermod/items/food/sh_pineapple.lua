ITEM.name = "Ananas"
ITEM.desc = "Un ananas aussi dur qu'une pierre avec le logo de l'Union."
ITEM.model = "models/bioshockinfinite/hext_pineapple.mdl"
ITEM.healthRestore = 5
ITEM.staminaRestore = 10
ITEM.acro = 0.5
ITEM.endu = 0.5
ITEM.stre = 1
ITEM.stam = 0.5
ITEM.price = 10