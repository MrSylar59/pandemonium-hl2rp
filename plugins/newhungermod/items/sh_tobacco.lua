ITEM.name = "Cigarettes"
ITEM.desc = "Un paquet de cigarettes avec le logo de l'Union si gentillement fournit par vos bénéfacteurs."
ITEM.model = "models/closedboxshit.mdl"
ITEM.category = "Consommable"
ITEM.healthRestore = -20
ITEM.staminaRestore = -10
ITEM.functions.Fumer = {
	icon = "icon16/wand.png", 
	sound = "items/battery_pickup.wav",
	onRun = function(item) 
		local client = item.player
		local char = client:getChar()

		client:SetHealth(math.min(client:Health() + item.healthRestore, 100))
		client:setLocalVar("stm", math.min(client:getLocalVar("stm", 100) + item.staminaRestore, 100))

		if (client:getChar():getAttrib("acro", 1) < 100) then
			client:getChar():updateAttrib("acro", 0.5)
		end

		if (client:getChar():getAttrib("end", 1) > 0) then		
			client:getChar():updateAttrib("end", -1)
		end

		if (client:getChar():getAttrib("str", 1) < 100) then
			client:getChar():updateAttrib("str", 0.5)
		end	
		
		if (client:getChar():getAttrib("acro", 1) > 0) then
			client:getChar():updateAttrib("stm", -1)
		end

		timer.Simple(0, function()
			for k, v in pairs(item.items) do
				if (IsValid(client) and client:getChar() and !client:getChar():getInv():add(v)) then
					nut.item.spawn(v, position)
				end
			end
		end)
	end
}
ITEM.permit = "food"
ITEM.items = {"empty_tobacco"}
ITEM.price = 5