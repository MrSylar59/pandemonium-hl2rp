PLUGIN.name = "Faim"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute un système de faim complet et innovant."

if (SERVER) then
	function PLUGIN:PostPlayerLoadout(client)
		local maxAttribs = nut.config.get("maxAttribs", 0)

		if (client:Team() != FACTION_OW) then
			if (timer.Exists("nutHunger" .. client:UniqueID())) then
				timer.Remove("nutHunger" .. client:UniqueID()) 
				timer.Create("nutHunger" .. client:UniqueID(), 2700, 0, function()
					hook.Run("PlayerIsHungry", client)
				end)
			else
				timer.Create("nutHunger" .. client:UniqueID(), 2700, 0, function()
					hook.Run("PlayerIsHungry", client)
				end)
			end
		else
			if (client:getChar():getAttrib("acro", 0) < maxAttribs) then
				client:getChar():setAttrib("acro", 100)
			end
			if (client:getChar():getAttrib("end", 0) < maxAttribs) then
				client:getChar():setAttrib("end", 100)
			end
			if (client:getChar():getAttrib("str", 0) < maxAttribs) then
				client:getChar():setAttrib("str", 100)
			end
			if (client:getChar():getAttrib("stm", 0) < maxAttribs) then
				client:getChar():setAttrib("stm", 100)
			end
		end
	end


	function PLUGIN:PlayerIsHungry(client) 

		if (client:getChar():getAttrib("acro", 0) > 0) then
			client:getChar():updateAttrib("acro", -1)
		else
			client:getChar():setAttrib("acro", 0)
		end

		if (client:getChar():getAttrib("end", 0) > 0) then
			client:getChar():updateAttrib("end", -1)
		else
			client:getChar():setAttrib("end", 0)
		end

		if (client:getChar():getAttrib("str", 0) > 0) then
			client:getChar():updateAttrib("str", -1)
		else
			client:getChar():setAttrib("str", 0)
		end
	
		if (client:getChar():getAttrib("stm", 0) > 0) then
			client:getChar():updateAttrib("stm", -1)
		else
			client:getChar():setAttrib("stm", 0)
		end
		
		client:notifyLocalized("Vous avez faim, vos attributs baissent...")
	end
end