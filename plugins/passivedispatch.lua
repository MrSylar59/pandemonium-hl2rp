PLUGIN.name = "Annonces Passives"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute des annonces passives se jouant lors d'un certain lapse de temps."

nut.config.add("passiveDispatch", true, "Active ou désactive les annonces passives.", nil, {
	category = "chat"
})

if (SERVER) then 

	function isPassiveDispatchActive() 
		return nut.config.get("passiveDispatch")
	end

	function PLUGIN:EmitRandomDispatch()
		local randomDispatch = {
			"RELOCATION CITOYEN",
			"CONSPIRATION",
			"MISSION éCHOUE"
		}

		return randomDispatch[math.random(1, #randomDispatch)]
	end

	function PLUGIN:Tick() 
		local players = player.GetAll()
		if (!IsValid(players[1])) then return end
		local ply = players[1]
		local curTime = CurTime()

		if (isPassiveDispatchActive() == true and ply:getChar() != nil) then
			if (!self.nextDispatch) then
				self.nextDispatch = curTime + math.random(300, 900) -- Une répêtition toutes les 5 à 15 minutes
			end

			if (curTime >= self.nextDispatch) then
				self.nextDispatch = nil
				ply.canEventDispatch = true
				nut.chat.send(ply, "dispatch", self:EmitRandomDispatch())
				ply.canEventDispatch = nil
			end
		end
	end

end