PLUGIN.name = "Promotion"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute la possibilité de promouvoir d'autres joueurs"

function isCombineElite(client)
	return client:isCombineRank(SCHEMA.i2Ranks) or client:isCombineRank(SCHEMA.i1Ranks) or client:isCombineRank(SCHEMA.ofcRanks) or client:isCombineRank(SCHEMA.cmdRanks)
end

function promoteMpf(rank, target) 
	targetName = target:Name():sub(target:Name():find("-")+1)
	target:getChar():setName(SCHEMA.cpPrefix .. rank .. "-" .. targetName)
end

nut.command.add("promotempf", {
	syntax = "<string name>",
	onRun = function(client, arguments) 
		local target = nut.command.findPlayer(client, table.concat(arguments, " "))

		if (!client:isCombine()) then
			return "@notCombine"
		elseif (!isCombineElite(client)) then
			return "Vous devez être Officier pour faire ça !"
		elseif (!target:isCombine()) then
			return "Vous ne pouvez promouvoir que les MPF !"
		elseif (client == target) then
			return "Vous ne pouvez pas vous promouvoir vous-même !"
		elseif (IsValid(target) and target:getChar()) then
			if (target:isCombineRank(SCHEMA.rctRanks)) then
				promoteMpf("i5", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang d'unité i5."
			elseif (target:isCombineRank(SCHEMA.i5Ranks)) then
				promoteMpf("i4", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang d'unité i4."
			elseif (target:isCombineRank(SCHEMA.i4Ranks)) then
				promoteMpf("i3", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang d'unité i3."
			elseif (target:isCombineRank(SCHEMA.i3Ranks)) then
				promoteMpf("i2", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang d'Officier i2."
			elseif (target:isCombineRank(SCHEMA.i2Ranks) and (client:isCombineRank(SCHEMA.i1Ranks) or client:isCombineRank(SCHEMA.ofcRanks) or client:isCombineRank(SCHEMA.cmdRank))) then
				promoteMpf("i1", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang d'Officier i1."
			elseif (target:isCombineRank(SCHEMA.i1Ranks) and (client:isCombineRank(SCHEMA.ofcRanks) or client:isCombineRank(SCHEMA.cmdRanks))) then
				promoteMpf("OfC", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang d'Officier supérieur."
			elseif (target:isCombineRank(SCHEMA.ofcRanks) and client:isCombineRank(SCHEMA.cmdRanks)) then
				promoteMpf("CmD", target)
				nut.log.add(client:Name() .. " a promu " .. target:Name())
				return "Vous avez promu " .. target:Name() .. " au rang de Commandant."
			else
				return "Vous ne pouvez offrir une promotion supérieur à la votre !"
			end
		end
	end
})

nut.command.add("demotempf", {
	syntax = "<string name>",
	onRun = function(client, arguments) 
		local target = nut.command.findPlayer(client, table.concat(arguments, " "))

		if (!client:isCombine()) then
			return "@notCombine"
		elseif (!isCombineElite(client)) then
			return "Vous devez être Officier pour faire ça !"
		elseif (!target:isCombine()) then
			return "Vous ne pouvez rétrograder que les MPF !"
		elseif (client == target) then
			return "Vous ne pouvez pas vous rétrograder vous-même !"
		elseif (IsValid(target) and target:getChar()) then
			if (target:isCombineRank(SCHEMA.rctRanks)) then
				return target:Name() .. " est déjà au rang minimal de la Protection Civile !"
			elseif (target:isCombineRank(SCHEMA.i5Ranks)) then
				promoteMpf("RCT", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé " .. target:Name() .. " au rang de recrue."
			elseif (target:isCombineRank(SCHEMA.i4Ranks)) then
				promoteMpf("i5", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé " .. target:Name() .. " au rang d'unité i5."
			elseif (target:isCombineRank(SCHEMA.i3Ranks)) then
				promoteMpf("i4", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé " .. target:Name() .. " au rang d'unité i4."
			elseif (target:isCombineRank(SCHEMA.i2Ranks) and (client:isCombineRank(SCHEMA.i1Ranks) or client:isCombineRank(SCHEMA.ofcRanks) or client:isCombineRank(SCHEMA.cmdRank))) then
				promoteMpf("i3", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé " .. target:Name() .. " au rang d'unité i3."
			elseif (target:isCombineRank(SCHEMA.i1Ranks) and (client:isCombineRank(SCHEMA.ofcRanks) or client:isCombineRank(SCHEMA.cmdRanks))) then
				promoteMpf("i1", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé " .. target:Name() .. " au rang d'Officier i2."
			elseif (target:isCombineRank(SCHEMA.ofcRanks) and client:isCombineRank(SCHEMA.cmdRanks)) then
				promoteMpf("i1", target)
				nut.log.add(client:Name() .. " a rétrogradé " .. target:Name())
				return "Vous avez rétrogradé " .. target:Name() .. " au rang d'Officier i1."
			else
				return "Vous ne pouvez rétrograder ni un supérieur ni un égal !"
			end
		end
	end
})