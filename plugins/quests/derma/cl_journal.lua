local PLUGIN = PLUGIN

local PANEL = {} -- La fenêtre du journal de quête du joueur

	function PANEL:Init() 
		self:SetTitle("Journal de Quête")
		self:SetPos(ScrW()/2, ScrH()/2)
		self:SetSize(ScrW() * 500/1920, ScrH() * 650/1080)
		self:MakePopup()
		self:SetContentAlignment(5)

		local noticePanel = self:Add("nutNoticeBar")
		noticePanel:Dock(TOP)
		noticePanel:DockMargin(0, 0, 0, 5)
		noticePanel:setType(7)
		noticePanel:setText("Vous trouverez ci-dessous les quêtes que vous suivez.")

		self.list = self:Add("DScrollPanel")
		self.list:Dock(FILL)
		self.list:SetDrawBackground(true)

		if (LocalPlayer():GetQuestCount() == 0) then
			local label = self.list:Add("DLabel")
			label:Dock(TOP)
			label:DockMargin(5, 5, 5, 5)
			label:SetFont("nutSmallFont")
			label:SetText("Vous n'avez aucune quête active.")
			label:SizeToContents()
		end

		for k, v in SortedPairs(LocalPlayer():GetQuests()) do
			local category = self.list:Add("DCollapsibleCategory")
			category:Dock(TOP)
			category:SetLabel(v.name)
			category:DockMargin(5, 5, 5, 5)
			category:SetPadding(5)

			local list = vgui.Create("DListLayout")
			list.Paint = function(list, w, h) 
				surface.SetDrawColor(0, 0, 0, 25)
				surface.DrawRect(0, 0, w, h)
			end
			category:SetContents(list)

			local questLabel = list:Add("DLabel")
			questLabel:Dock(TOP)
			questLabel:DockMargin(5, 5, 5, 5)
			questLabel:SetFont("nutSmallFont")
			questLabel:SetText(v.desc)
			questLabel:SizeToContents()

			local rewardsLabel = list:Add("DLabel")
			rewardsLabel:Dock(TOP)
			rewardsLabel:DockMargin(5, 10, 5, 5)
			rewardsLabel:SetFont("nutSmallFont")
			rewardsLabel:SetText("Récompenses :")
			rewardsLabel:SizeToContents()

			for rType, reward in pairs(v.rewards) do
				if (rType == "tokens") then
					local tokens = list:Add("DLabel")
					tokens:Dock(TOP)
					tokens:DockMargin(5, 5, 5, 5)
					tokens:SetFont("nutSmallFont")
					tokens:SetText("Tokens : "..reward)
					tokens:SizeToContents()
				elseif (rType == "items") then
					for _, items in pairs(reward) do
						for cat, data in pairs(items) do 
							if (cat == "uid") then
								local tableItem = nut.item.list[data]
								local item = list:Add("DLabel")
								item:Dock(TOP)
								item:DockMargin(5, 5, 5, 5)
								item:SetFont("nutSmallFont")
								item:SetText("   - "..tableItem.name)
								item:SizeToContents()
							end
						end
					end
				end
			end

			local validate = list:Add("DButton")
			validate:Dock(TOP)
			validate:DockMargin(50, 5, 50, 10)
			validate:SetFont("nutSmallFont")
			validate:SetText("Rendre Quête")
			validate:SetTall(25)
			validate:SetEnabled(false)
			if (LocalPlayer():CanCompleteQuest(v.uniqueID, LocalPlayer():GetQuest(v.uniqueID))) then
				validate:SetEnabled(true)
			end
			validate.DoClick = function() 
				if (!validate:IsEnabled()) then
					return
				end
				netstream.Start("nutCompleteQuest", v.uniqueID)
				validate:SetEnabled(false)
			end
		end
	end

	function PANEL:Think()
		if (!self:IsActive()) then
			self:MakePopup()
		end
	end

vgui.Register("nut_Journal", PANEL, "DFrame")