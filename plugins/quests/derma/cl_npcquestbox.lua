local PLUGIN = PLUGIN

local PANEL = {} -- La fenêtre dans laquelle le joueur peut choisir une quête

	function PANEL:Init() 
		local ply = LocalPlayer()

		self:SetTitle("")
		self:SetPos(ScrH()/2, ScrW()/2)
		self:SetSize(ScrW() * 650/1920, ScrH() * 700/1080)
		self:MakePopup()
		self:SetContentAlignment(5)

		local noticePanel = self:Add("nutNoticeBar")
		noticePanel:Dock(TOP)
		noticePanel:DockMargin(0, 0, 0, 5)
		noticePanel:setType(7)
		noticePanel:setText("Vous trouverez ci-dessous une quête que vous pouvez accepter.")

		if (ply:CanTakeNewQuest()) then

			timer.Simple(0.1, function() 
				local quest = PLUGIN:GetQuest(ply:GetNextQuest(self.ent:getFaction()))

				if (!quest) then
					if (self.ent:getFaction() == "loyaliste") then
						local label = self:Add("DLabel")
						label:Dock(TOP)
						label:DockMargin(5, 20, 5, 5)
						label:SetFont("nutSmallFont")
						label:SetText("\"Désolé, je n'ai plus aucune mission à vous confier, vous devriez déposer une candidature pour rejoindre la CWU.\"")
						label:SetWrap(true)
						label:SetAutoStretchVertical(true)
					elseif (self.ent:getFaction() == "rebelle") then
						local label = self:Add("DLabel")
						label:Dock(TOP)
						label:DockMargin(5, 20, 5, 5)
						label:SetFont("nutSmallFont")
						label:SetText("\"Désolé, je n'ai plus aucune mission à vous confier, vous devriez chercher dans des endroits peu fréquentés par la milice, vous trouverez sûrement des personnes qui penses comme vous...\"")
						label:SetWrap(true)
						label:SetAutoStretchVertical(true)
					else
						local label = self:Add("DLabel")
						label:Dock(TOP)
						label:DockMargin(5, 20, 5, 5)
						label:SetFont("nutSmallFont")
						label:SetText("\"Désolé, je n'ai plus aucune mission à vous confier, vous devriez rejoindre la Protection Civile, renseignez-vous auprès de la CWU.\"")
						label:SetWrap(true)
						label:SetAutoStretchVertical(true)
					end
				else
					self.list = self:Add("DScrollPanel")
					self.list:Dock(FILL)
					self.list:SetDrawBackground(true)
					
					local questName = self.list:Add("DLabel")
					questName:Dock(TOP)
					questName:DockMargin(self:GetWide()/2 - questName:GetWide(), 20, 5, 5)
					questName:SetFont("nutMediumFont")
					questName:SetText(quest.name)
					questName:SizeToContents()

					local questText = self.list:Add("DLabel")
					questText:Dock(TOP)
					questText:DockMargin(5, 10, 5, 5)
					questText:SetFont("nutSmallFont")
					questText:SetText(quest.text)
					questText:SetWrap(true)
					questText:SetAutoStretchVertical(true)

					local questButton = self:Add("DButton")
					questButton:Dock(BOTTOM)
					questButton:DockMargin(150, 5, 150, 5)
					questButton:SetFont("nutSmallFont")
					questButton:SetText("Accepter Quête")
					questButton:SetTall(25)
					questButton.DoClick = function() 
						if (!questButton:IsEnabled()) then
							return
						end
						netstream.Start("nutAcceptQuest", self.ent:getFaction(), quest.uniqueID)
						questButton:SetEnabled(false)
						self:Close()
					end
				end
			end)

		else
			local label = self:Add("DLabel")
			label:Dock(TOP)
			label:DockMargin(5, 20, 5, 5)
			label:SetFont("nutSmallFont")
			label:SetText("\"Hmm... Je vous aurai bien demandé quelque chose, mais vous me semblez être quelqu'un de déjà fort occupé.\"")
			label:SetWrap(true)
			label:SetAutoStretchVertical(true)
		end
	end

	function PANEL:Setup(ent) 
		local ply = LocalPlayer()
		self.ent = ent
		self:SetTitle(self.ent:getNetVar("name", ""))

		if (self.ent:getFaction() == "rebelle") then
			if (self.ent:isFemale()) then
				ply:EmitSound("vo/npc/female01/answer10.wav", 75, math.random(95, 105))
			else
				ply:EmitSound("vo/npc/male01/answer10.wav", 75, math.random(95, 105))
			end
		elseif (self.ent:getFaction() == "loyaliste") then
			if (self.ent:isFemale()) then
				ply:EmitSound("vo/npc/female01/busy02.wav", 75, math.random(95, 105))
			else
				ply:EmitSound("vo/npc/male01/busy02.wav", 75, math.random(95, 105))
			end
		else
			ply:EmitSound("npc/metropolice/vo/on"..math.random(1, 2)..".wav", math.random(60, 70), math.random(80, 120))
			timer.Simple(.5, function() 
				ply:EmitSound("npc/metropolice/vo/citizen.wav")
				timer.Simple(1, function() 
					ply:EmitSound("npc/metropolice/vo/off"..math.random(1, 3)..".wav", math.random(60, 70), math.random(80, 120))
				end)
			end)
		end
	end

	function PANEL:Think()
		local ent = self.ent

		if (!IsValid(ent)) then
			self:Remove()
			return
		end

		if ((self.nextUpdate or 0) < CurTime()) then
			self:SetTitle(self.ent:getNetVar("name", ""))
			self.nextUpdate = CurTime() + .25
		end
	end

	function PANEL:OnRemove() 
		if (IsValid(nut.gui.questEdit)) then
			nut.gui.questEdit:Remove()
		end
	end

vgui.Register("nut_Quest", PANEL, "DFrame")