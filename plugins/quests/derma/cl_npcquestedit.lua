local PLUGIN = PLUGIN

local PANEL = {} -- La fenêtre dans laquelle les admins peuvent modifier les infos d'un PNJ

	function PANEL:Init()

		local ent = nut.gui.quest.ent

		self:SetTitle("Config PNJ Quête")
		self:SetPos(0, ScrH()/2 - 210)
		self:MoveLeftOf(nut.gui.quest, 350)
		self:SetSize(ScrW() * 400/1920, ScrH() * 420/1080)
		self:MakePopup()
		self:SetContentAlignment(5)

		local noticePanel = self:Add("nutNoticeBar")
		noticePanel:Dock(TOP)
		noticePanel:DockMargin(0, 0, 0, 5)
		noticePanel:setType(7)
		noticePanel:setText("Complétez les différents champs puis validez.")

		self.list = self:Add("DScrollPanel")
		self.list:Dock(FILL)
		self.list:SetDrawBackground(true)

		local nameLabel = self.list:Add("DLabel")
		nameLabel:Dock(TOP)
		nameLabel:DockMargin(5, 5, 5, 5)
		nameLabel:SetText("Nom :")
		nameLabel:SizeToContents()

		local nameEntry = self.list:Add("DTextEntry")
		nameEntry:Dock(TOP)
		nameEntry:DockMargin(5, 5, 55, 5)
		nameEntry:SetText(ent:getNetVar("name", ""))
		nameEntry:SetTall(20)
		nameEntry.OnEnter = function(this) 
			if (ent:getNetVar("name", "") != this:GetText()) then
				self:UpdateNPC("name", this:GetText())
			end
		end

		local descLabel = self.list:Add("DLabel")
		descLabel:Dock(TOP)
		descLabel:DockMargin(5, 15, 5, 5)
		descLabel:SetText("Description :")
		descLabel:SizeToContents()

		local descEntry = self.list:Add("DTextEntry")
		descEntry:Dock(TOP)
		descEntry:DockMargin(5, 5, 55, 5)
		descEntry:SetText(ent:getNetVar("desc", ""))
		descEntry:SetTall(20)
		descEntry.OnEnter = function(this) 
			if (ent:getNetVar("desc", "") != this:GetText()) then
				self:UpdateNPC("desc", this:GetText())
			end
		end

		local modelLabel = self.list:Add("DLabel")
		modelLabel:Dock(TOP)
		modelLabel:DockMargin(5, 15, 5, 5)
		modelLabel:SetText("Description :")
		modelLabel:SizeToContents()

		local modelEntry = self.list:Add("DTextEntry")
		modelEntry:Dock(TOP)
		modelEntry:DockMargin(5, 5, 55, 5)
		modelEntry:SetText(ent:GetModel():lower())
		modelEntry:SetTall(20)
		modelEntry.OnEnter = function(this) 
			if (ent:GetModel():lower() != this:GetText():lower()) then
				self:UpdateNPC("model", this:GetText():lower())
			end
		end

		local factionLabel = self.list:Add("DLabel")
		factionLabel:Dock(TOP)
		factionLabel:DockMargin(5, 15, 5, 5)
		factionLabel:SetText("Faction :")
		factionLabel:SizeToContents()

		local factionEntry = self.list:Add("DComboBox")
		factionEntry:Dock(TOP)
		factionEntry:DockMargin(5, 5, 55, 5)
		factionEntry:SetValue(ent:getNetVar("faction", "aucune"))
		factionEntry:AddChoice("loyaliste")
		factionEntry:AddChoice("rebelle")
		factionEntry:AddChoice("mpf")
		factionEntry.OnSelect = function(panel, index, value) 
			if (ent:getNetVar("faction") != value) then
				self:UpdateNPC("faction", value)
			end
		end

		local minAlignSlider = self.list:Add("DNumSlider")
		minAlignSlider:Dock(TOP)
		minAlignSlider:DockMargin(5, 15, 5 ,5)
		minAlignSlider:SetSize(200, 50)
		minAlignSlider:SetText("Alignement Minimal :")
		minAlignSlider:SetMin(-100)
		minAlignSlider:SetMax(100)
		minAlignSlider:SetDecimals(0)
		minAlignSlider:SetValue(ent:getNetVar("minAlign", "-100"))
		minAlignSlider.OnValueChanged = function(panel, value) 
			if (ent:getNetVar("minAlign") != math.floor(value)) then
				self:UpdateNPC("minAlign", math.floor(value))
			end
		end

		local maxAlignSlider = self.list:Add("DNumSlider")
		maxAlignSlider:Dock(TOP)
		maxAlignSlider:DockMargin(5, 5, 5 ,5)
		maxAlignSlider:SetSize(200, 20)
		maxAlignSlider:SetText("Alignement Maximal :")
		maxAlignSlider:SetMin(-100)
		maxAlignSlider:SetMax(100)
		maxAlignSlider:SetDecimals(0)
		maxAlignSlider:SetValue(ent:getNetVar("maxAlign", "100"))
		maxAlignSlider.OnValueChanged = function(panel, value) 
			if (ent:getNetVar("maxAlign") != math.floor(value)) then
				self:UpdateNPC("maxAlign", math.floor(value))
			end
		end
	end

	function PANEL:UpdateNPC(key, value) 
		netstream.Start("npcEdit", key, value)
	end

	function PANEL:OnRemove()
		if (IsValid(nut.gui.quest)) then
			nut.gui.quest:Remove()
		end
	end

vgui.Register("nut_QuestEdit", PANEL, "DFrame")