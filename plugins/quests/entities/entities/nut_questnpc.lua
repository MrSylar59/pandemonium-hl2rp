ENT.Type = "anim"
ENT.PrintName = "NPC Quête"
ENT.Category = "NutScript"
ENT.Spawnable = true
ENT.AdminOnly = true

function ENT:Initialize() 
	if (SERVER) then
		self:SetModel("models/Humans/Group01/Male_04.mdl")
		self:SetUseType(SIMPLE_USE)
		self:SetMoveType(MOVETYPE_NONE)
		self:DrawShadow(true)
		self:SetSolid(SOLID_BBOX)
		self:PhysicsInit(SOLID_BBOX)

		self:setNetVar("name", "Jean Lasalle")
		self:setNetVar("desc", "")
		self:setNetVar("faction", "aucune")
		self:setNetVar("minAlign", "-100")
		self:setNetVar("maxAlign", "100")

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:EnableMotion(false)
			physObj:Sleep()
		end
	end

	timer.Simple(1, function() 
		if (IsValid(self)) then
			self:setAnim()
		end
	end)
end

function ENT:setAnim() 
	for k, v in ipairs(self:GetSequenceList()) do
		if (v:lower():find("idle") and v != "idlenoise") then
			return self:ResetSequence(k)
		end
	end

	self:ResetSequence(4)
end

function ENT:getFaction() 
	return self:getNetVar("faction")
end

function ENT:isFemale()
	local model = self:GetModel():lower()

	return model:find("female") or model:find("alyx") or model:find("mossman") or nut.anim.getModelClass(model) == "citizen_female"
end

if (SERVER) then
	local PLUGIN = PLUGIN

	function ENT:SpawnFunction(client, trace) 
		local angles = (trace.HitPos - client:GetPos()):Angle()
		angles.r = 0
		angles.p = 0
		angles.y = angles.y + 180

		local entity = ents.Create("nut_questnpc")
		entity:SetPos(trace.HitPos)
		entity:SetAngles(angles)
		entity:Spawn()

		PLUGIN:saveQuestNPCs()

		return entity
	end

	function ENT:Use(activator) 
		if (activator:Team() == FACTION_CITIZEN or activator:IsAdmin()) then
			if (activator:getChar():getData("align", 0) < tonumber(self:getNetVar("minAlign")) or activator:getChar():getData("align", 0) > tonumber(self:getNetVar("maxAlign"))) then
				if (self:getFaction() == "mpf") then
					nut.chat.send(activator, "it", "L'unité vous jette un regard sombre, vous sentez un frisson vous parcourir et tournez les talons.")
				else
					nut.chat.send(activator, "it", "La personne vous regarde d'un aire méfiant et refuse de vous parler.")
				end
			elseif (activator:IsAdmin()) then
				activator.npcQuest = self
				netstream.Start(activator, "nut_QuestWindow", self:EntIndex())
			else
				activator.npcQuest = self
				netstream.Start(activator, "nut_QuestWindow", self:EntIndex())
			end
		else
			if (self:getFaction() == "mpf") then
				nut.chat.send(activator, "it", "L'unité se tient fièrement devant vous, restant parfaitement impassible.")
			else
				nut.chat.send(activator, "it", "La personne vous regarde d'un aire interrogatif.")
			end
		end
	end

	function ENT:OnRemove() 
		if (!nut.shuttingDown and !nut.cleaningUp and !self.nutIsSafe) then
			PLUGIN:saveQuestNPCs()
		end
	end

else

	local TEXT_OFFSET = Vector(0, 0, 20)
	local toScreen = FindMetaTable("Vector").ToScreen
	local drawText = nut.util.drawText
	local getConfig = nut.config.get

	ENT.DrawEntityInfo = true

	function ENT:onDrawEntityInfo(alpha) 
		local pos = toScreen(self.LocalToWorld(self, self.OBBCenter(self)) + TEXT_OFFSET)
		local x, y = pos.x, pos.y
		local desc = self.getNetVar(self, "desc")

		drawText(self.getNetVar(self, "name", "Jean Lasalle"), x, y, ColorAlpha(getConfig("color"), alpha), 1, 1, nil, alpha * 0.65)

		if (desc) then
			drawText(desc, x, y + 16, ColorAlpha(color_white, alpha), 1, 1, "nutSmallFont", alpha * 0.65)
		end
	end

end