local PLUGIN = PLUGIN
PLUGIN.name = "Quêtes"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute un système de quêtes complet dans le Gamemode de base"

PLUGIN.curQuests = {}
PLUGIN.quests = {}
PLUGIN.questsObj = {}

local entityMeta = FindMetaTable("Entity")
local playerMeta = FindMetaTable("Player")

function PLUGIN:RegisterQuest(str, data) 
	if (self.quests[str]) then
		print("[Quest Plugin] - Erreur, la quête avec le UID "..str.." existe déjà.")
		return false
	else
		self.quests[str] = data
		return true
	end
end
nut.util.include("sh_quests.lua")

function PLUGIN:RegisterQuestObj(str, data) 
	if (self.questsObj[str]) then
		print("[Quest Plugin] - Erreur, la quête avec le UID "..str.." a déjà des objectifs.")
		return false
	elseif (!self.quests[str]) then
		print("[Quest Plugin] - Erreur, aucune quête avec le UID "..str.." n'a été trouvée.")
		return false
	else
		self.questsObj[str] = data
		return true
	end
end
nut.util.include("sh_questsobj.lua")

function playerMeta:GetQuests() 
	return self:getNetVar("questAssigned", {})
end

function playerMeta:GetQuest(str) 
	return self:GetQuests()[str]
end

function playerMeta:GetQuestCount() 
	local count = 0

	for _ in pairs(self:GetQuests()) do
		count = count + 1
	end

	return count
end

function playerMeta:CanTakeNewQuest() 

	if (self:getChar():getData("nextQuestTime", 0) > os.time() or self:GetQuestCount() == nut.config.get("maxSimQuests")) then
		return false
	end

	return true 
end

function playerMeta:SetLastQuest(faction, uid) 
	if (faction == "rebelle") then
		self:getChar():setData("lastQuestReb", uid)
	elseif (faction == "loyaliste") then
		self:getChar():setData("lastQuestLoy", uid)
	elseif (faction == "mpf") then
		self:getChar():setData("lastQuestMpf", uid)
	end
end

function playerMeta:GetNextQuest(faction) 
	local lastQuestReb = self:getChar():getData("lastQuestReb", "")
	local lastQuestLoy = self:getChar():getData("lastQuestLoy", "")
	local lastQuestMpf = self:getChar():getData("lastQuestMpf", "")

	print("Last : " .. lastQuestReb, lastQuestReb:sub(5))
	print("faction : ".. faction)

	if (faction == "rebelle") then
		if (lastQuestReb == "") then
			return "reb_1"
		end
		nextQuestReb = lastQuestReb:sub(5) + 1
		return "reb_" .. nextQuestReb
	elseif (faction == "loyaliste") then
		if (lastQuestLoy == "") then
			return "loy_1"
		end
		nextQuestLoy = lastQuestLoy:sub(5) + 1
		return "loy_" .. nextQuestLoyb
	else
		if (lastQuestMpf == "") then
			return "mpf_1"
		end
		nextQuestMpf = lastQuestMpf:sub(5) + 1
		return "mpf_" .. nextQuestMpfb
	end
end

function playerMeta:AddQuest(str, data) 
	local quests = self:GetQuests()
	data = data or {}

	if (self:GetQuestCount() == nut.config.get("maxSimQuests")) then
		return false -- Le joueur a le nombre maximal de quête
	else
		quests[str] = data
		self:setNetVar("questAssigned", quests)
		self:getChar():setData("nextQuestTime", os.time() + 60) -- TODO : Changer le temps pour attendre plus d'une minute
		return true
	end
end

function playerMeta:CanCompleteQuest(str, data)
	local quest = PLUGIN:GetQuestObj(str)
	return quest:CanComplete(self, data)
end

function playerMeta:RemoveQuest(str) 
	local quests = self:GetQuests()

	if (quests[str]) then
		quests[str] = nil
		self:setNetVar("questAssigned", quests)
		return true
	else
		return false
	end
end

function playerMeta:GiveQuestReward(str) 
	local quest = PLUGIN:GetQuestObj(str)
	local reward = PLUGIN:GetQuestReward(str)
	local qData = self:GetQuest(str)

	if (quest and reward) then
		for k, v in pairs(reward) do
			if (k == "tokens") then
				self:getChar():giveMoney(v)
			elseif (k == "items") then
				for _, data in pairs(v) do
					self:getChar():getInv():add(data.uid, data.amount, data.data or {})
				end
			end
		end

		quest:RemoveQuestItem(self, qData)
		quest:PostReward(self, qData)
	end
end

function PLUGIN:GetQuests() 
	return self.quests
end

function PLUGIN:GetQuest(str) 
	return self:GetQuests()[str]
end

function PLUGIN:GetQuestReward(str) 
	return self.quests[str].rewards
end

function PLUGIN:GetQuestsObj()
	return self.questsObj
end

function PLUGIN:GetQuestObj(str) 
	return self:GetQuestsObj()[str]
end

if (SERVER) then

	function PLUGIN:SaveData() 
		self:SaveQuests()
	end

	function PLUGIN:SaveQuests() 
		local data = nut.data.get("curQuests", self.curQuests)

		for _, ply in pairs(player.GetAll()) do
			if (ply:GetQuests()) then
				data[ply:GetName()] = {steamID = ply:SteamID64(), quest = ply:GetQuests()}
			end
		end

		nut.data.set("curQuests", data)
	end

	function PLUGIN:saveQuestNPCs() 
		local data = {}

		for k, v in ipairs(ents.FindByClass("nut_questnpc")) do
			data[#data + 1] = {
				name = v:getNetVar("name"),
				desc = v:getNetVar("desc"),
				faction = v:getNetVar("faction"),
				minAlign = v:getNetVar("minAlign"),
				maxAlign = v:getNetVar("maxAlign"),
				pos = v:GetPos(),
				ang = v:GetAngles(),
				model = v:GetModel()
			}
		end

		self:setData(data)
	end

	function PLUGIN:LoadData() 
		local data = self:getData() or {}

		for k, v in ipairs(data) do
			local entity = ents.Create("nut_questnpc")
			entity:SetPos(v.pos)
			entity:SetAngles(v.ang)
			entity:SetModel(v.model)
			entity:Spawn()
			entity:setNetVar("name", v.name)
			entity:setNetVar("desc", v.desc)
			entity:setNetVar("faction", v.faction)
			entity:setNetVar("minAlign", v.minAlign)
			entity:setNetVar("maxAlign", v.maxAlign)

			if (v.faction == "loyaliste") then
				if (v.model:find("zelpa")) then
					entity:SetBodygroup(1, 12)
				end
			end
		end
	end

	function PLUGIN:PlayerSpawn(ply) 
		local questData = nut.data.get("curQuests", {})
		local quests = {}

		ply:setNetVar("questAssigned", nil)

		for k, v in pairs(questData) do
			if (k == ply:GetName()) then
				if (v.steamID == ply:SteamID64()) then
					for k2, v2 in pairs(v.quest) do
						quests[v2.uniqueID] = v2
						ply:setNetVar("questAssigned", quests)
					end
				end
			end
		end
	end

	function PLUGIN:PlayerDisconnected(ply) 
		self:SaveQuests()
	end

	netstream.Hook("nutAcceptQuest", function(client, faction, quest) 
		client:AddQuest(quest, PLUGIN:GetQuest(quest))
		client:SetLastQuest(faction, quest)
		client:notifyLocalized("Vous avez accepté une quête ! (/journal)")
		PLUGIN:SaveQuests()
	end)

	netstream.Hook("nutCompleteQuest", function(client, quest) 
		client:GiveQuestReward(quest)
		client:RemoveQuest(quest)
		client:notifyLocalized("Vous avez rendu une quête, vous avez reçu vos récompenses !")
		PLUGIN:SaveQuests()
	end)

	netstream.Hook("npcEdit", function(client, key, data) 
		if (client:IsAdmin()) then
			local ent = client.npcQuest

			if (!IsValid(ent)) then
				return
			end

			if (key == "name") then
				ent:setNetVar("name", data)
			elseif (key == "desc") then
				ent:setNetVar("desc", data)
			elseif (key == "faction") then
				ent:setNetVar("faction", data)
			elseif (key == "minAlign") then
				ent:setNetVar("minAlign", data)
			elseif (key == "maxAlign") then
				ent:setNetVar("maxAlign", data)
			elseif (key == "model") then
				ent:SetModel(data)
				ent:setAnim()
			end
		end

		PLUGIN:saveQuestNPCs()
	end)

else

	netstream.Hook("nut_journal", function() 
		if (IsValid(nut.gui.journal)) then
			nut.gui.journal:Remove()
			return
		end
		nut.gui.journal = vgui.Create("nut_Journal")
		nut.gui.journal:Center()
	end)

	netstream.Hook("nut_QuestWindow", function(index) 
		local ent = Entity(index)

		if (!IsValid(ent)) then
			return
		end

		if (IsValid(nut.gui.quest)) then
			nut.gui.quest:Remove()
			return
		end
		nut.gui.quest = vgui.Create("nut_Quest")
		nut.gui.quest:Center()
		nut.gui.quest:Setup(ent)

		if (LocalPlayer():IsAdmin()) then
			if (IsValid(nut.gui.questedit)) then
				nut.gui.questedit:Remove()
				return
			end
			nut.gui.questEdit = vgui.Create("nut_QuestEdit")
		end
	end)

end

nut.config.add("maxSimQuests", 2, "Le nombre maximal de quêtes qu'un joueur peut prendre en même temps.", nil, {
	data = {min = 1, max = 5},
	category = "server"
})

nut.command.add("journal", {
	onRun = function(client) 
		if (IsValid(client)) then
			netstream.Start(client, "nut_journal")
		end
	end
})