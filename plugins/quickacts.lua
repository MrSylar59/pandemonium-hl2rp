PLUGIN.name = "Menu Acts"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute un sous menu sur la touche F4 pour des acts."

if (SERVER) then
	function PLUGIN:ShowSpare2(client) 
		if (client:getChar()) then
			netstream.Start(client, "actMenu")
		end
	end
end

if (CLIENT) then
	netstream.Hook("actMenu", function()
		local menu = DermaMenu()

		menu:AddOption("Tomber au sol", function()
			nut.command.send("fallover")
		end)
		menu:AddOption("S'asseoire", function() 
			nut.command.send("actsit")
		end)
		menu:AddOption("S'allonger", function()
			nut.command.send("actinjured")
		end)
		menu:AddOption("Lever les mains", function()
			nut.command.send("actarrest")
		end)
		menu:AddOption("Acclamer", function()
			nut.command.send("actcheer")
		end)
		menu:AddOption("Faire signe de suivre", function()
			nut.command.send("acthere")
		end)
		menu:AddOption("S'adosser à un mur", function()
			nut.command.send("actsitwall")
		end)
		menu:AddOption("Attendre debout", function()
			nut.command.send("actstand")
		end)

		menu:Open()
		menu:MakePopup()
		menu:Center()
	end)
end