PLUGIN.name = "Bonus Aléatoire (rations)"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Permet de reçevoir un objet aléatoire ou non dans sa ration."

function PLUGIN:GetRandomItem()
	local randomObject = math.random(0, 100)

	if (randomObject <= 10) then -- On a 10% de chance d'avoir des cacahuètes
		return "peanuts"
	elseif (randomObject > 10 and randomObject <= 15) then -- On a 5% de chance d'avoir des chips
		return "crisps"
	elseif (randomObject > 15 and randomObject <= 20) then -- On a 5% de chance d'avoir des cigarettes
		return "tobacco"
	elseif (randomObject > 20 and randomObject <= 25) then -- On a 5% de chance d'avoir un livre
		return "blackcat"
	elseif (randomObject > 25 and randomObject <= 26) then -- On a 1% de chance d'avoir du chocolat
		return "chocolate"
	else -- On a 74% de chance de ne rien avoir... :'(
		return nil
	end
end