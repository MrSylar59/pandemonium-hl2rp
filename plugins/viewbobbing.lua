PLUGIN.name = "Balancement"
PLUGIN.author = "Black Tea & MrSylar59"
PLUGIN.desc = "Ajoute un effet de balancement à la vue du joueur"

nut.config.add("viewbob", true, "Active ou désactive l'effet de balancement.", nil, {
	category = "characters"
})

if (CLIENT) then

	local NUT_CVAR_VIEWBOB = CreateClientConVar("nut_vb_enabled", "1", true)

	local curAng = Angle(0, 0, 0)
	local curVec = Vector(0, 0, 0)
	local tarAng = Angle(0, 0, 0)
	local tarVec = Vector(0, 0, 0)

	function isAllowed() 
		return nut.config.get("viewbob")
	end

	function PLUGIN:SetupQuickMenu(menu) 
		if (isAllowed()) then
			local button = menu:addCheck("Effet de balancement", function(panel, state) 
				if (state) then
					RunConsoleCommand("nut_vb_enabled", "1")
				else
					RunConsoleCommand("nut_vb_enabled", "0")
				end
			end, NUT_CVAR_VIEWBOB:GetBool())
		end
	end

	function PLUGIN:CalcView(ply, pos, ang, fov) 
		local ft = FrameTime() -- CurTime() nécessaire pour déssiner la dernière frame (en secondes)
		local vel = math.floor(ply:GetVelocity():Length())
		local runSpeed = nut.config.get("runSpeed")
		local walkSpeed = nut.config.get("walkSpeed")
		local view = {}

		if (ply:IsValid() and ply:Alive() and ply:getChar() and !ply:ShouldDrawLocalPlayer() and (!ply:getLocalVar("ragdoll") or ply:getLocalVar("ragdoll") == 0) and NUT_CVAR_VIEWBOB:GetBool() and isAllowed() and ply:GetMoveType() != MOVETYPE_NOCLIP) then
			if (ply:IsOnGround()) then
				if (vel > walkSpeed + 5) then -- Quand le joueur cours, l'écran est plus secoué
					local perc = vel / runSpeed * 100
					perc = math.Clamp(perc, 0.5, 6)
					tarAng = Angle(math.abs(math.cos(CurTime() * (runSpeed/40)) * 1.5 * perc) - perc, math.sin(CurTime() * (runSpeed/40)) * 0.2 * perc, 0)
					tarVec = Vector(0, 0, math.sin(CurTime() *(runSpeed/30)) * 0.4 * perc)
				else -- Ici, on marche donc c'est plus léger
					local perc = vel / walkSpeed * 100
					perc = math.Clamp(perc / 30, 0.5, 4)
					tarAng = Angle(math.cos(CurTime()*4) * .3 * perc, 0, 0)
					tarVec = Vector(0, 0, math.sin(CurTime()*3) * .3 * perc)
				end
			else
				if (ply:WaterLevel() > 2) then
					tarAng = Angle(0, 0, 0)
					tarVec = Vector(0, 0, 0)
				else
					local vel = math.abs(ply:GetVelocity().z)
					local af = 0
					local perc = math.Clamp( vel/200, .1, 8 )

					if (perc > 1) then
						af = perc
					end

					tarAng = Angle(math.cos(CurTime() * 15) * 2 * perc + math.Rand(-af * 2, af * 2), math.sin(CurTime() * 15) * 2 * perc + math.Rand(-af * 2, af * 2) ,math.Rand(-af * 5, af * 5))
					tarVec = Vector(math.cos(CurTime() * 15) * .5 * perc , math.sin(CurTime() * 15) * .5 * perc, 0 )
				end
			end
			-- On édite notre vue
			curAng = LerpAngle(ft * 10, curAng, tarAng)
			curVec = LerpVector(ft * 10, curVec, tarVec)

			-- On donne notre vue
			view.angles = ang + curAng
			view.origin = pos + curVec
			view.fov = fov

			return view
		end
	end
end