PLUGIN.name = "Réalité Virtuelle"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Ajoute un côté réalité virtuelle pour les MPF."

local playerMeta = FindMetaTable("Player")

function playerMeta:GetThreatLevel(mpf) 
	local model = self:GetModel():lower()
	local weapon = self:GetActiveWeapon()
	local distance = self:GetPos():DistToSqr(mpf:GetPos())/65536 * 255

	if (!self:Alive() or !IsValid(weapon)) then
		return
	end

	if (!self:isCombine() and !model:find("metropolice") and !model:find("scanner")) then
		if (self:isWepRaised() or (distance < 75 and !self:isCwu())) then
			if (weapon:GetClass() != "nut_hands" or distance < 25) then
				return 3
			else
				return 2
			end
		else
			return 1
		end
	else
		return 0
	end
end

if (CLIENT) then

	function PLUGIN:PreDrawHalos() 
		local client = LocalPlayer()

		if (client:isCombine()) then
			local combines = {}
			local citizens = {}
			local aCitizens = {}
			local aCivil = {}

			for k, v in pairs(player.GetAll()) do
				if (v != client) then
					local threat = v:GetThreatLevel(client)

					if (threat == 1) then
						table.insert(citizens, v)
					elseif (threat == 2) then
						table.insert(aCitizens, v)
					elseif (threat == 3) then
						table.insert(aCivil, v)
					elseif (threat == 0) then
						table.insert(combines, v)
					end
				end
			end

			for k, v in pairs(ents.GetAll()) do
				if (v:GetClass() == "prop_ragdoll") then
					if (v:GetModel():lower():find("metropolice")) then
						table.insert(combines, v)
					else
						table.insert(citizens, v)
					end
				end
			end

			halo.Add(citizens, Color(0, 255, 0))
			halo.Add(aCitizens, Color(255, 140, 0))
			halo.Add(aCivil, Color(255, 0, 0))
			halo.Add(combines, Color(0, 0, 255))
		end
	end

end