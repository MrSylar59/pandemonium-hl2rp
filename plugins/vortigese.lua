PLUGIN.name = "Langage Vortigaunt"
PLUGIN.author = "MrSylar59"
PLUGIN.desc = "Permet à joueur vortigaunt de parler en vort."

local playerMeta = FindMetaTable("Player")
local vortWords = {
	"ah’", "ahglah", "ahhhr", "ai", "araa", "azh", "bahh", "ban",
	"beelees", "beh", "bot", "chaa", "chol", "chua", "chee", "daa",
	"daal", "dara", "dam", "darr", "dech", "del", "eek", "eesk",
	"eeya", "egh", "eez", "eh'", "ell", "esh", "ga'", "gurrah",
	"gruk", "grun", "gu", "goh", "ha", "hach", "hee", "hagh", "ho",
	"hu", "hoy-hoy", "ih'", "ihr", "jah", "jhi", "joh", "kahh", 
	"kallah", "keh", "ket", "ksaa", "kraa", "kut", "kth", "kuu",
	"laa", "lahh", "lach", "lan", "lees", "leh", "lud", "luu", "lihluu",
	"loo", "mei", "meh", "mech", "mii", "min", "mirr", "mit", "morr",
	"nae", "nach", "nahh", "noh", "nohaa", "ny", "oa", "odu", "ogh",
	"ohm", "ozh", "raa", "raeh", "rach", "ree", "rex", "rihx", "rog",
	"saa", "seh", "shaa", "shii", "shuk", "sun", "stu", "tahh", "tak",
	"talash", "tao", "tash", "tay", "terr", "uua", "ula", "urr", "uya",
	"vih", "vin", "voo", "vort", "voosh", "vra", "vurr", "xih", "xen",
	"xil", "xkah", "yee", "yem", "yit", "yox", "zhey", "zich", "zoo",
	"zuk", "zurr"
}
local vortSounds = {
	"vo/npc/vortigaunt/vortigese02.wav",
	"vo/npc/vortigaunt/vortigese03.wav",
	"vo/npc/vortigaunt/vortigese04.wav",
	"vo/npc/vortigaunt/vortigese05.wav",
	"vo/npc/vortigaunt/vortigese07.wav",
	"vo/npc/vortigaunt/vortigese08.wav",
	"vo/npc/vortigaunt/vortigese09.wav",
	"vo/npc/vortigaunt/vortigese11.wav",
	"vo/npc/vortigaunt/vortigese12.wav"
}

function playerMeta:isVortigaunt() 
		return self:GetModel():lower():find("vortigaunt")
end

local function VortigauntSentence(text) 
	local words = string.Split(text, " ")
	local newSentence = ""

	for k, word in pairs(words) do
		if (word != ("!" and "?")) then
			local randomWord = table.Random(vortWords)

			if (k == 1) then
				local firstWord = randomWord:sub(2)
				local maj = randomWord[1]:upper()

				randomWord = maj .. firstWord
			end
			newSentence = newSentence .." ".. randomWord
		else
			newSentence = newSentence .. word
		end 
	end

	return newSentence
end

if (CLIENT) then

	function PLUGIN:OnChatReceived(client, chatType, text, anonymous) 
		if (chatType == "vort" and client:isVortigaunt()) then
			return text
		elseif (chatType == "vort" and !client:isVortigaunt()) then
			return VortigauntSentence(text)
		end
	end

end

nut.chat.register("vort", {
	format = "%s dit en Vortigese: \"%s\"",
	onCanSay = function(speaker, text) 
		if (speaker:isVortigaunt()) then
			speaker:EmitSound(table.Random(vortSounds), 75, math.random(70, 80))
		else
			speaker:notifyLocalized("Vous ne connaissez pas le langage des vortigaunts.")
			return false
		end
	end,
	prefix = {"/vort"}
})