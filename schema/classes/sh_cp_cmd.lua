CLASS.name = "le Commandant"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 48
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.cmdRanks)
end

CLASS_CP_CMD = CLASS.index