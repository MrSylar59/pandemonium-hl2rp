CLASS.name = "une Élite de la Protection Civile"
CLASS.desc = "Les meilleurs officiers de la PC."
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.eliteRanks)
end

CLASS_CP_ELITE = CLASS.index