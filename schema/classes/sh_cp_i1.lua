CLASS.name = "un Officier i1"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 32
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.i1Ranks)
end

CLASS_CP_I1 = CLASS.index