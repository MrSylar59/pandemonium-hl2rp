CLASS.name = "un Officier i2"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 24
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.i2Ranks)
end

CLASS_CP_I2 = CLASS.index