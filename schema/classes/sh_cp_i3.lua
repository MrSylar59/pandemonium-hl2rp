CLASS.name = "une unité i3"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 20
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.i3Ranks)
end

CLASS_CP_I3 = CLASS.index