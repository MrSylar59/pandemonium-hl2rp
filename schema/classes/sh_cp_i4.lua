CLASS.name = "une unité i4"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 16
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.i4Ranks)
end

CLASS_CP_I4 = CLASS.index