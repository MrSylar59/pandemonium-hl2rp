CLASS.name = "une unité i5"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 8
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.i5Ranks)
end

CLASS_CP_I5 = CLASS.index