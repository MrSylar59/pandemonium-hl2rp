CLASS.name = "un Officier Supérieur"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 120
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.ofcRanks)
end

CLASS_CP_OFC = CLASS.index