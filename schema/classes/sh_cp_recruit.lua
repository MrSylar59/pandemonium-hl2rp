CLASS.name = "une recrue"
CLASS.desc = "Au plus bas de la PC."
CLASS.pay = 4
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.rctRanks)
end

CLASS_CP_RCT = CLASS.index