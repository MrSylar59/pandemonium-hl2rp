CLASS.name = "une Unité de la Protection Civile"
CLASS.desc = "Une unité de la PC standard."
CLASS.faction = FACTION_CP

function CLASS:onCanBe(client)
	return client:isCombineRank(SCHEMA.unitRanks)
end

CLASS_CP_UNIT = CLASS.index