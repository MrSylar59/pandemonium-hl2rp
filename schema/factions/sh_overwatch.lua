FACTION.name = "fOverwatchName"
FACTION.desc = "fOverwatchDesc"
FACTION.color = Color(181, 94, 94)
FACTION.isDefault = false
FACTION.models = {
	"models/dpfilms/metropolice/rtb_police.mdl"
}
FACTION.pay = 30
FACTION.isGloballyRecognized = true

function FACTION:onGetDefaultName(client)
	if (SCHEMA.digitsLen >= 1) then
		local digits = math.random(tonumber("1"..string.rep("0", SCHEMA.digitsLen-1)), tonumber(string.rep("9", SCHEMA.digitsLen)))
		return SCHEMA.ovPrefix.."."..digits, true
	else
		return SCHEMA.ovPrefix, true
	end
end

FACTION_OW = FACTION.index