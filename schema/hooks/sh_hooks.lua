function SCHEMA:CanPlayerEditData(client, target)
	if (client:isCombine()) then
		return true
	end

	return false
end

if (CLIENT) then
	local canBeep = true
	function SCHEMA:ChatTextChanged(text)
		local textLength = text:len()
		local usingCommand = false

		if (text[1] == "!" or text[1] == "/" or text[1] == "@") then
			usingCommand = true
			if (text:sub(1, 2) == "/y" or text:sub(1, 2) == "/w" or text:sub(1, 3) == "/ra") then
				usingCommand = false
			end
		end

		if (LocalPlayer():isCombine() and text:len() == 3 and !usingCommand and canBeep) then
			netstream.Start("Beep")
			canBeep = false
		end
	end

	function SCHEMA:FinishChat()
		canBeep = true
	end
else
	netstream.Hook("Beep", function(client)
		if(client:Team() == FACTION_OW) then 
			faction = "combine_soldier"
		else
			faction = "metropolice"
		end
		client:EmitSound("npc/".. faction .."/vo/on"..math.random(1, 2)..".wav", math.random(70, 80), math.random(80, 120))
	end)
end;