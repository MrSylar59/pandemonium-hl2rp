ITEM.name = "Petit Sac"
ITEM.desc = "Un petit sac pouvant transporter quelques objets."
ITEM.invWidth = 2
ITEM.invHeight = 2
ITEM.price = 25
ITEM.permit = "misc"
ITEM.model = "models/props_c17/BriefCase001a.mdl"
ITEM.category = "Stockage"