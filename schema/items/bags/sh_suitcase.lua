ITEM.name = "Valise"
ITEM.desc = "Une valise marron qui permet de stocker un peu plus."
ITEM.model = "models/props_c17/suitcase_passenger_physics.mdl"
ITEM.invWidth = 4
ITEM.invHeight = 3
ITEM.price = 50
ITEM.category = "Stockage"
ITEM.permit = "misc"