ITEM.name = "Carte ID"
ITEM.desc = "Un morceau de plastique permettant d'identifier quelqu'un."
ITEM.model = "models/dorado/tarjeta2.mdl"
ITEM.factions = {FACTION_CP, FACTION_ADMIN}
ITEM.functions.Assigner = {
	onRun = function(item)
		local data = {}
			data.start = item.player:EyePos()
			data.endpos = data.start + item.player:GetAimVector()*96
			data.filter = item.player
		local entity = util.TraceLine(data).Entity

		if (IsValid(entity) and entity:IsPlayer() and entity:Team() == FACTION_CITIZEN) then
			local status, result = item:transfer(entity:getChar():getInv():getID())

			if (status) then
				item:setData("name", entity:Name())
				item:setData("id", math.random(10000, 99999))
				
				return true
			else
				item.player:notify(result)
			end
		end

		return false
	end,
	onCanRun = function(item)
		return item.player:isCombine()
	end
}

function ITEM:getDesc()
	local description = self.desc.."\nCette CID est assignée à "..self:getData("name", "personne")..", #"..self:getData("id", "00000").."."

	if (self:getData("cwu")) then
		description = description.."\nCette carte possède un tampon prioritaire."
	end

	return description
end