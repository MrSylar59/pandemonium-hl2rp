ITEM.name = "Ration Pour Citoyen"
ITEM.desc = "Un petit paquet de ration contenant de la nouriture et de l'eau."
ITEM.model = "models/weapons/w_packatc.mdl"
ITEM.width = 1
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(-4, 5.9, 200),
	ang = Angle(90, 0, 0),
	fov = 5.5
}
ITEM.functions.Ouvrir = {
	icon = "icon16/briefcase.png",
	sound = "physics/body/body_medium_impact_soft1.wav",
	onRun = function(item)
		local position = item.player:getItemDropPos()
		local client = item.player
		local randomItem = hook.Run("GetRandomItem")

		timer.Simple(0, function()
			for k, v in pairs(item.items) do
				if (IsValid(client) and client:getChar() and !client:getChar():getInv():add(v)) then
					nut.item.spawn(v, position)
				end
			end
		end)

		if (IsValid(client) and client:getChar() and randomItem and !client:getChar():getInv():add(randomItem)) then
			nut.item.spawn(randomItem, position)
		end

		client:getChar():giveMoney(12)
	end
}
ITEM.items = {"water", "ration_packet"} 
ITEM.factions = FACTION_CP