ITEM.name = "RPG"
ITEM.desc = "Un lance-roquette téléguidée."
ITEM.model = "models/weapons/w_rocket_launcher.mdl"
ITEM.class = "weapon_rpg"
ITEM.weaponCategory = "primary"
ITEM.price = 950
ITEM.width = 3
ITEM.height = 1
ITEM.iconCam = {
	pos = Vector(-19.607843399048, 200, 2),
	ang = Angle(0, 270, 0),
	fov = 16
}
ITEM.flag = "Y"