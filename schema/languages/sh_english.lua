LANGUAGE = {
	fCitizenName = "Citoyen",
	fCitizenDesc = "Un simple citoyen vivant sous le joug du Cartel.",
	fCopName = "Protection_Civile",
	fCopDesc = "Un protecteur de cette institution civile.",
	fOverwatchName = "Milicien",
	fOverwatchDesc = "Un soldat de l'Union Universelle.",
	fAdminName = "Administrateur",
	fAdminDesc = "Le dirigeant de la cité.",
	-- This is an example of how you can have your schema description be translatable.
	-- But it's commented because people keep complaining that their description isn't changing.
	-- So this is commented since it overwrites the SCHEMA.desc
	--schemaDesc = "Under rule of the Universal Union.",
	mustBeCP = "Vous devez être un membre de la Protection Civile pour faire ça.",
	objectives = "Objectifs",
	notCombine = "Vous devez être un membre de la Milice pour faire ça.",
	prioritySet = "Vous avez changer le statut prioritaire de %s.",
	noReqDev = "Vous ne disposez d'aucun dispositif de requête.",
	CitoyenTip = "Un simple citoyen vivant sous le joug du Cartel.",
	Protection_CivileTip = "Un protecteur de cette institution civile.",
	MilicienTip = "Un soldat de l'Union Universelle.",
	AdministrateurTip = "Le dirigeant de la Cité.",
	CWUTip = "La partie administrative de la Cité"
}
