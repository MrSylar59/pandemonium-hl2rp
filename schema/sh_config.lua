-- Customize the beep sounds here, before and after voices.
SCHEMA.beepSounds = {}
SCHEMA.beepSounds[FACTION_CP] = {
	on = {
		"npc/overwatch/radiovoice/on1.wav",
		"npc/overwatch/radiovoice/on3.wav",
		"npc/metropolice/vo/on2.wav"
	},
	off = {
		"npc/metropolice/vo/off1.wav",
		"npc/metropolice/vo/off2.wav",
		"npc/metropolice/vo/off3.wav",
		"npc/metropolice/vo/off4.wav",
		"npc/overwatch/radiovoice/off2.wav",
		"npc/overwatch/radiovoice/off2.wav"
	}
}
SCHEMA.beepSounds[FACTION_OW] = {
	on = {
		"npc/combine_soldier/vo/on1.wav",
		"npc/combine_soldier/vo/on2.wav"
	},
	off = {
		"npc/combine_soldier/vo/off1.wav",
		"npc/combine_soldier/vo/off2.wav",
		"npc/combine_soldier/vo/off3.wav"
	}
}

-- Sounds play after the player has died.
SCHEMA.deathSounds = {}
SCHEMA.deathSounds[FACTION_CP] = {
	"npc/metropolice/die1.wav",
	"npc/metropolice/die2.wav",
	"npc/metropolice/die3.wav",
	"npc/metropolice/die4.wav"
}
SCHEMA.deathSounds[FACTION_OW] = {
	"npc/combine_soldier/die1.wav",
	"npc/combine_soldier/die2.wav",
	"npc/combine_soldier/die3.wav"
}

-- Sounds the player makes when injured.
SCHEMA.painSounds = {}
SCHEMA.painSounds[FACTION_CP] = {
	"npc/metropolice/pain1.wav",
	"npc/metropolice/pain2.wav",
	"npc/metropolice/pain3.wav",
	"npc/metropolice/pain4.wav"
}
SCHEMA.painSounds[FACTION_OW] = {
	"npc/combine_soldier/pain1.wav",
	"npc/combine_soldier/pain2.wav",
	"npc/combine_soldier/pain3.wav"
}

-- Civil Protection name prefix.
SCHEMA.cpPrefix = "MPF.C17:"

-- OverWatch Soldiers name prefix.
SCHEMA.ovPrefix = "OTA.OWS:EpU-ECHO"

-- How long the Combine digits are.
SCHEMA.digitsLen = 3

-- Rank information.
SCHEMA.rctRanks = {"RCT"}
--SCHEMA.unitRanks = {"i5", "i4", "i3", "i2", "i1"}
SCHEMA.scnRanks = {"SCN", "CLAW.SCN"}
SCHEMA.i5Ranks = {"i5"}
SCHEMA.i4Ranks = {"i4"}
SCHEMA.i3Ranks = {"i3"}
SCHEMA.i2Ranks = {"i2"}
SCHEMA.i1Ranks = {"i1"}
SCHEMA.eliteRanks = {"EpU"}
SCHEMA.ofcRanks = {"OfC"}
SCHEMA.cmdRanks = {"CmD"}

-- What model each rank should be.
SCHEMA.rankModels = {
	["RCT"] = "models/dpfilms/metropolice/retrocop.mdl",
	["i5"] = "models/dpfilms/metropolice/retrocop.mdl",
	["i4"] = "models/dpfilms/metropolice/retrocop.mdl",
	["i3"] = "models/dpfilms/metropolice/retrocop.mdl",
	["i2"] = "models/dpfilms/metropolice/retrocop.mdl",
	["i1"] = "models/dpfilms/metropolice/retrocop.mdl",
	["EpU"] = "models/dpfilms/metropolice/rtb_police.mdl",
	["OfC"] = "models/dpfilms/metropolice/blacop.mdl",
	["CmD"] = "models/dpfilms/metropolice/phoenix_police.mdl",
	["SCN"] = "models/combine_scanner.mdl",
	["CLAW.SCN"] = "models/shield_scanner.mdl"
}

-- The default player data when using /data
SCHEMA.defaultData = [[
Points: 0
Statut: Citoyen

Infractions:
------------------------------------
UNITÉ INFRACTION POINTS
------------------------------------
ex: 123 / Violation Audio / 1
]]